#!/bin/sh
cd `dirname $0`

if [ "$1" = "set" ] ; then
	timestamp=$(date +%s)
else
	timestamp=000
fi
sed -E -i '' 's/((js|css)\?v=)[0-9]+/\1'$timestamp'/g' production/index.html
sed -E -i '' 's/(let VERSION_TIMESTAMP = ")[0-9]+(";)/\1'$timestamp'\2/g' production/sw.js js/main.js
