let VERSION_TIMESTAMP = "000";
let mainCacheUrls = [
	"/",
	"css.css?v="+VERSION_TIMESTAMP,
	"js.js?v="+VERSION_TIMESTAMP,
	"3dRenderWorker.js?v="+VERSION_TIMESTAMP,
];

self.addEventListener("install", e => {
	e.waitUntil(onInstall());
});

async function onInstall(){
	let cache =  await caches.open("mainCacheV"+VERSION_TIMESTAMP);
	await cache.addAll(mainCacheUrls.map(url => new Request(url, {credentials: 'same-origin'})));
	await self.skipWaiting();
}

self.addEventListener("fetch", e => {
	if(VERSION_TIMESTAMP === "000") return;
	let url = new URL(e.request.url);
	let path = url.pathname+url.search;
	if(path.startsWith("/") && path != "/") path = path.slice(1);
	if(mainCacheUrls.includes(path) || path.endsWith(".svg")){
		e.respondWith(getUrlFromCache(e));
	}
});

async function getUrlFromCache(e){
	let response = await caches.match(e.request);
	if(response) return response;

	let response2 = await fetch(e.request);
	if(response2.ok){
		let cache = await caches.open("mainCacheV"+VERSION_TIMESTAMP);
		cache.put(e.request, response2.clone())
	}
	return response2;
}

self.addEventListener("activate", e => {
	e.waitUntil(onActivate());
});

async function onActivate(){
	//clear old caches
	let keys = await caches.keys();
	for(const key of keys){
		let result = /mainCacheV(\d+)/.exec(key);
		if(result && result[1] != VERSION_TIMESTAMP){
			await caches.delete(key);
		}
	}
}
