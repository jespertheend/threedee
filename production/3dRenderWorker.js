let activeTasks = [];
let renderingPaused = false;
let renderSettings = {
	tearLength: 5,
	holeFillSettings: [
		{type: "smart"}
	],
};

onmessage = (e) => {
	let opCode = e.data.opCode;
	if(opCode == 0){ //new render
		activeTasks.push(e.data);
	}else if(opCode == 1){ //clear all
		for(const task of activeTasks){
			postMessage({
				rgbData: null,
				renderId: task.renderId,
			})
		}
		activeTasks = [];
	}else if(opCode == 2){ //pause rendering
		renderingPaused = true;
	}else if(opCode == 3){ //continue rendering
		renderingPaused = false;
	}else if(opCode == 4){ //cancel specific render
		let id = e.data.id;
		for(let i=activeTasks.length -1; i>=0; i--){
			let task = activeTasks[i];
			if(task && task.renderId == id){
				postMessage({
					rgbData: null,
					renderId: id,
				});
				activeTasks.splice(i, 1);
			}
		}
	}else if(opCode == 5){ //render settings
		renderSettings = e.data.settings;
	}
}

setInterval(_ => {
	if(renderingPaused) return;
	if(activeTasks.length > 0){
		let data = activeTasks[0];
		activeTasks.shift();
		let rgbData = data.rgbData;
		let depthData = data.depthData;
		let width = data.width;
		let height = data.height;
		let depthAmount = data.depthAmount;
		for(let i=0; i<height; i++){
			let start = i * width;
			let intValues = new Uint8ClampedArray(rgbData, start * 4, width * 4);
			let depthValues = new Uint8ClampedArray(depthData, start * 4, width * 4);
			renderRow(intValues, depthValues, depthAmount);
		}
		let renderId = data.renderId;
		postMessage({
			rgbData: rgbData,
			renderId: renderId,
		}, [rgbData])
	}
}, 32);

function getHoleFillTypeForLength(length){
	let type = {
		type: "color",
		color: [0,0,0]
	};
	for(const setting of renderSettings.holeFillSettings){
		if(!setting.minLength || length > setting.minLength){
			type = setting;
		}
	}
	return type;
}

function renderRow(intValues, depthValues, depthAmount){
	let width = intValues.length/4;
	let newPixels = new Array(width);
	for(let i=0; i<width; i++){
		newPixels[i] = [];
	}
	for(let i=0; i < width - 1; i++){
		let r1 = intValues[i*4];
		let g1 = intValues[i*4+1];
		let b1 = intValues[i*4+2];
		let d1 = 255 - depthValues[i*4];
		let r2 = intValues[i*4+4];
		let g2 = intValues[i*4+5];
		let b2 = intValues[i*4+6];
		let d2 = 255 - depthValues[i*4+4];

		let xPos1 = calcNewXPosForPixel(i, d1, depthAmount);
		let xPos2 = calcNewXPosForPixel(i + 1, d2, depthAmount);
		if(xPos1 == xPos2){
			if(xPos1 > 0 && xPos1 < width){
				let pixel;
				if(d1 < d2){
					pixel = [r1,g1,b1,d1,i];
				}else{
					pixel = [r2,g2,b2,d2,i+1];
				}
				newPixels[xPos1].push(pixel);
			}
		}else{
			let xPosDist = Math.abs(xPos1 - xPos2);
			if(renderSettings.tearLength >= 0 && xPosDist > renderSettings.tearLength){ //only render first pixel, don't interpolate
				if(xPos1 > 0 && xPos1 < width){
					newPixels[xPos1].push([
						r1, //r
						g1, //g
						b1, //b
						d1, //z depth
						i,
					]);
				}
			}else{
				let lerpToNegative = xPos1 > xPos2;
				for(let j=0; j <= xPosDist; j++){
					let lerpValue = j/xPosDist;
					let xPos = xPos1;
					if(lerpToNegative){
						xPos -= j;
					}else{
						xPos += j;
					}
					if(xPos > 0 && xPos < width){
						let originalXPos = lerpValue < 0.5 ? i : i+1;
						newPixels[xPos].push([
							r1 + lerpValue * (r2-r1), //r
							g1 + lerpValue * (g2-g1), //g
							b1 + lerpValue * (b2-b1), //b
							d1 + lerpValue * (d2-d1), //z depth
							originalXPos,
						]);
					}
				}
			}
		}
	}
	let holes = [];
	let prevFilledStack = true;
	for(let i=0; i<width; i++){
		let pixelStack = newPixels[i];
		let filledStack = pixelStack.length > 0;
		if(filledStack != prevFilledStack){
			holes.push(i);
			prevFilledStack = filledStack;
		}
	}
	for(let i=0; i<holes.length; i+=2){
		let start = holes[i];
		let end = holes[i+1];
		if(!end) end = width;
		let holeWidth = end - start;
		let fillType = getHoleFillTypeForLength(holeWidth);
		if(fillType.type == "smart"){
			fillHoleSmart(newPixels, start, end, width, depthValues, depthAmount);
		}else if(fillType.type == "color"){
			fillHoleColor(newPixels, start, end, fillType.color);
		}else if(fillType.type == "lerp"){
			fillHoleLerp(newPixels, start, end);
		}
	}
	for(let i=0; i<width; i++){
		let closestDepthPixel = getHighestPixelFromStack(newPixels[i]);
		if(closestDepthPixel){
			intValues[i*4] = closestDepthPixel[0];
			intValues[i*4+1] = closestDepthPixel[1];
			intValues[i*4+2] = closestDepthPixel[2];
		}else{
			intValues[i*4] = 0;
			intValues[i*4+1] = 0;
			intValues[i*4+2] = 0;
		}
	}
}

function fillHoleColor(newPixels, start, end, color){
	for(let i = start; i < end; i++){
		newPixels[i] = [[
			color[0], //r
			color[1], //g
			color[2], //b
			0, //z depth
			-1
		]];
	}
}

function fillHoleLerp(newPixels, start, end){
	let leftPixel = getHighestPixelFromStack(newPixels[start - 1]);
	let rightPixel = getHighestPixelFromStack(newPixels[end]);
	if(leftPixel && !rightPixel){
		rightPixel = leftPixel;
	}else if(!leftPixel && rightPixel){
		leftPixel = rightPixel;
	}else if(!leftPixel && !rightPixel){
		return;
	}
	let r1 = leftPixel[0];
	let g1 = leftPixel[1];
	let b1 = leftPixel[2];
	let z1 = leftPixel[3];
	let r2 = rightPixel[0];
	let g2 = rightPixel[1];
	let b2 = rightPixel[2];
	let z2 = rightPixel[3];
	let holeLength = end - start;
	for(let j=0; j<holeLength; j++){
		let lerpValue = (j+1)/(holeLength+1);
		newPixels[start + j] = [[
			r1 + lerpValue * (r2-r1), //r
			g1 + lerpValue * (g2-g1), //g
			b1 + lerpValue * (b2-b1), //b
			z1 + lerpValue * (z2-z1), //z depth
			-1
		]];
	}
}

function fillHoleSmart(newPixels, start, end, width, depthValues, depthAmount){
	let leftPixelStack = newPixels[start - 1];
	let rightPixelStack = newPixels[end];
	let leftPixel = getHighestPixelFromStack(leftPixelStack);
	let rightPixel = getHighestPixelFromStack(rightPixelStack);
	let holeLength = end - start;
	if(leftPixel && rightPixel){
		let leftHoleX = leftPixel[4]+1;
		let rightHoleX = rightPixel[4]-1;
		if(leftHoleX < 0 || rightHoleX < 0){
			fixHoleFromOneSide(newPixels, start, end, holeLength, false);
		}else if(leftHoleX >= width || rightHoleX >= width){
			fixHoleFromOneSide(newPixels, start, end, holeLength, true);
		}else{
			let leftHoleDepth = 255 - depthValues[leftHoleX*4];
			let leftIsForwardFacing = leftHoleDepth < leftPixel[3];

			let rightHoleDepth = 255 - depthValues[rightHoleX*4];
			let rightIsForwardFacing = rightHoleDepth < rightPixel[3];

			if(leftIsForwardFacing && rightIsForwardFacing){
				let leftMirrorData = getMirrorData(newPixels, start - 1, -1, holeLength);
				let rightMirrorData = getMirrorData(newPixels, end, 1, holeLength);
				for(let j=0; j<holeLength; j++){
					let leftMirrorPixel = leftMirrorData[j];
					let lr = leftMirrorPixel[0];
					let lg = leftMirrorPixel[1];
					let lb = leftMirrorPixel[2];
					let ld = leftMirrorPixel[3];
					let rightMirrorPixel = rightMirrorData[holeLength - j - 1];
					let rr = rightMirrorPixel[0];
					let rg = rightMirrorPixel[1];
					let rb = rightMirrorPixel[2];
					let rd = rightMirrorPixel[3];
					let lerpValue = (j+1)/(holeLength+1);;
					let lerpedPixel = [
						lr + lerpValue * (rr-lr), //r
						lg + lerpValue * (rg-lg), //g
						lb + lerpValue * (rb-lb), //b
						ld + lerpValue * (rd-ld), //z depth
						-1,
					]
					newPixels[j + start] = [lerpedPixel];
				}
			}else if(!leftIsForwardFacing && !rightIsForwardFacing){
				let leftHoleOriginalXPos = calcNewXPosForPixel(leftHoleX, leftHoleDepth, depthAmount);
				leftHoleOriginalXPos = Math.max(leftHoleOriginalXPos, 0);
				leftHoleOriginalXPos = Math.min(leftHoleOriginalXPos, width - 1);
				let leftMirrorData = getMirrorData(newPixels, leftHoleOriginalXPos, 1, holeLength, leftHoleDepth);
				for(let j=0; j<holeLength; j++){
					let mirrorDataIndex = depthAmount > 0 ? j : holeLength - j - 1;
					newPixels[j + start] = [leftMirrorData[mirrorDataIndex]];
				}
			}else{
				fixHoleFromOneSide(newPixels, start, end, holeLength, leftIsForwardFacing);
			}
		}
	}else{
		fixHoleFromOneSide(newPixels, start, end, holeLength, !!leftPixel)
	}
}

function fixHoleFromOneSide(newPixels, start, end, holeLength, leftSide){
	if(leftSide){
		let mirrorData = getMirrorData(newPixels, start - 1, -1, holeLength);
		for(let j=0; j<holeLength; j++){
			newPixels[j + start] = [mirrorData[j]];
		}
	}else{
		let mirrorData = getMirrorData(newPixels, end, 1, holeLength);
		for(let j=0; j<holeLength; j++){
			newPixels[j + start] = [mirrorData[holeLength - j - 1]];
		}
	}
}

function getHighestPixelFromStack(pixelStack){
	if(!pixelStack) return null;
	let closestDepthPixel = null;
	for(const pixel of pixelStack){
		if(!closestDepthPixel || pixel[3] < closestDepthPixel[3]){
			closestDepthPixel = pixel;
		}
	}
	return closestDepthPixel;
}

function getClosestPixelFromStack(pixelStack, desiredDepth, maxDifference = -1){
	let closestDepthPixel = null;
	let closestDist = Infinity;
	for(const pixel of pixelStack){
		let dist = Math.abs(pixel[3] - desiredDepth);
		if(maxDifference > 0 && dist > maxDifference) continue;
		if(!closestDepthPixel || dist < closestDist){
			closestDepthPixel = pixel;
			closestDist = dist;
		}
	}
	return closestDepthPixel;
}

function calcNewXPosForPixel(originalXPos, pixelDepthAmount, globalDepthAmount){
	return originalXPos + Math.round((pixelDepthAmount - 128) * globalDepthAmount);
}

function copyPixel(pixel){
	return [pixel[0], pixel[1], pixel[2], pixel[3], pixel[4]];
}

function getMirrorData(newPixels, start, dir, length, desiredDepth = 0){
	start = Math.max(start, 0);
	start = Math.min(start, newPixels.length - 1);
	let currentClonePixelXPos = start;
	let startPixelStack = newPixels[start];
	let startPixel = getClosestPixelFromStack(startPixelStack, desiredDepth);
	let depth = startPixel == null ? 0 : startPixel[3];
	let mirrorData = new Array(length);
	for(let i=0; i<length; i++){
		let clonePixelStack = newPixels[currentClonePixelXPos];
		if(clonePixelStack.length > 0){
			let clonePixel = getClosestPixelFromStack(clonePixelStack, depth, 5);
			if(clonePixel){
				mirrorData[i] = copyPixel(clonePixel);
				depth = clonePixel[3];
			}
		}
		if((currentClonePixelXPos <= 0 && dir < 0) || (currentClonePixelXPos >= newPixels.length - 1 && dir > 0)){
			dir *= -1;
		}
		currentClonePixelXPos += dir;
	}
	//if mirroData is completely empty
	let containsFilledValue = false;
	for(let i=0; i<length; i++){
		if(mirrorData[i]){
			containsFilledValue = true;
			break;
		}
	}
	if(!containsFilledValue){
		for(let i=0; i<length; i++){
			mirrorData[i] = [0,0,0,255,-1];
		}
	}
	let holes = [];
	let prevValidPixel = true;
	for(let i=0; i<length; i++){
		let pixel = mirrorData[i];
		let validPixel = !!pixel;
		if(validPixel != prevValidPixel){
			holes.push(i);
			prevValidPixel = validPixel;
		}
	}
	for(let i=0; i<holes.length; i+=2){
		let holeStart = holes[i];
		let holeEnd = holes[i+1];
		if(!holeEnd) holeEnd = length;
		let lerpPixelStart = mirrorData[holeStart-1];
		let lerpPixelEnd = mirrorData[holeEnd];
		if(!lerpPixelStart) lerpPixelStart = lerpPixelEnd;
		if(!lerpPixelEnd) lerpPixelEnd = lerpPixelStart;
		let sr = lerpPixelStart[0];
		let sg = lerpPixelStart[1];
		let sb = lerpPixelStart[2];
		let sd = lerpPixelStart[3];
		let er = lerpPixelEnd[0];
		let eg = lerpPixelEnd[1];
		let eb = lerpPixelEnd[2];
		let ed = lerpPixelEnd[3];
		let holeLength = holeEnd - holeStart;
		for(let j=0; j<holeLength; j++){
			let lerpValue = (j+1)/(holeLength+1);
			mirrorData[j + holeStart] = [
				sr + lerpValue * (er-sr), //r
				sg + lerpValue * (eg-sg), //g
				sb + lerpValue * (eb-sb), //b
				sd + lerpValue * (ed-sd), //z depth
				-1
			];
		}
	}
	return mirrorData;
}
