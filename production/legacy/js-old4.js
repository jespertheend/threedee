var img,c,ctx,c2,ctx2,depthC,depthCtx,rootImgCanvas,rootImgData,settings,brushAmountGui;

$(document).ready(function(){
  $("#uploadScreen").click(function(){
    $("#upload").click();
  });

  $("#uploadScreen").on("dragover",function(e){
    e.stopPropagation();
    e.preventDefault();
  });

  $("#uploadScreen").on("drop",function(e){
    e.stopPropagation();
    e.preventDefault();
    readFile(e.originalEvent.dataTransfer.files[0]);
  });

  $("#upload").change(function(e){
    readFile(e.target.files[0]);
  });

  $("#uploadDepth").change(function(e){
    var reader = new FileReader();
    reader.readAsDataURL(e.target.files[0]);
    reader.onload = function(event){
      var depthImg = new Image();
      depthImg.onload = function(){
        depthCtx.drawImage(depthImg,0,0,depthC.width,depthC.height);
        $("#uploadDepth").replaceWith($("#uploadDepth").clone(true));
      }
      depthImg.src = event.target.result;
    }
  })

  theLoop();
});

function readFile(file){
  var reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = function(event){
    img = new Image();
    img.onload = function(){
      $("#uploadScreen").hide();
      $("#editScreen").show();

      c = document.createElement("canvas");
      c.id = "c";
      c.width = img.width;
      c.height = img.height;
      ctx = c.getContext("2d");

      c2 = document.createElement("canvas");
      c2.id = "c2";
      c2.width = img.width;
      c2.height = img.height;
      ctx2 = c2.getContext("2d");

      rootImgCanvas = document.createElement("canvas");
      rootImgCanvas.width = img.width;
      rootImgCanvas.height = img.height;
      var rootImgCtx = rootImgCanvas.getContext("2d");
      rootImgCtx.drawImage(img,0,0);
      rootImgData = rootImgCtx.getImageData(0,0,img.width,img.height).data;

      depthC = document.createElement("canvas");
      depthC.width = img.width;
      depthC.height = img.height;
      depthC.id = "overlayCanvas";
      depthCtx = depthC.getContext("2d");
      depthCtx.fillStyle = "rgb(127,127,127)";
      depthCtx.fillRect(0,0,img.width,img.height);

      $([c,c2,depthC]).addClass("c");
      $("#leftContainer").html(c);
      $("#rightContainer").html(c2);
      $("#overlayContainer").html(depthC);

      var guiSettings = function(){
        this.depthAlpha = 0.5;
        this.autoRender = false;
        this.render = function(){
          renderBoth(true);
        }
        this.undo = function(){
          if(depthBackup){
            depthCtx.drawImage(depthBackup,0,0);
          }
        }
        this.amount = 1;
        this.brushSize = 1;
        this.brushAmount = 1.4215686275;
        this.brushInfluence = 1;
        this.quality = 100;
        this.fixDoubleEdges = false;
        this.invertFront = false;
        this.globalScale = 1;
        this.globalXOffset = 0;
        this.globalYOffset = 0;
        this.xStart = 0;
        this.xEnd = 0;
        this.yStart = 0;
        this.yEnd = 0;
        this.pickColor = function(){
          isPickingColor = true;
        };
        this.alwaysPickColor = false;
        this.uploadDepth = function(){
          $("#uploadDepth").click();
        }
        this.exportType = 0;
        this.export = function(){
          if(!isRendered && settings.exportType != 1){
            exportAfterRender = true;
            settings.render();
          }else{
            exportAfterRender = false;
            if(settings.exportType == 0 || settings.exportType == 3){
              var crossViewCanvas = document.createElement("canvas");
              crossViewCanvas.width = c.width*2;
              crossViewCanvas.height = c.height;
              var crossViewCtx = crossViewCanvas.getContext("2d");
              var ca1 = c;
              var ca2 = c2;
              if(settings.exportType == 3){
                ca1 = c2;
                ca2 = c;
              }
              crossViewCtx.drawImage(ca1,0,0);
              crossViewCtx.drawImage(ca2,c.width,0);
              window.open(crossViewCanvas.toDataURL());
            }
            if(settings.exportType == 1)
              window.open(depthC.toDataURL());
            if(settings.exportType == 2 || settings.exportType == 4){
              var glassesCanvas = document.createElement("canvas");
              glassesCanvas.width = c.width;
              glassesCanvas.height = c.height;
              var glassesCtx = glassesCanvas.getContext("2d");
              glassesCtx.drawImage(c,0,0);
              var glassesCanvasRight = document.createElement("canvas");
              glassesCanvasRight.width = c.width;
              glassesCanvasRight.height = c.height;
              var glassesCtxRight = glassesCanvasRight.getContext("2d");
              glassesCtxRight.drawImage(c2,0,0);

              var glassesData = glassesCtx.getImageData(0,0,img.width,img.height);
              var glassesDataRight = glassesCtxRight.getImageData(0,0,img.width,img.height).data;
              if(settings.exportType == 4){
                grayScale(glassesData.data);
                grayScale(glassesDataRight);
              }
              for(var i=0; i<glassesData.data.length; i++){
                var cl = i%4;
                if(cl == 0){
                  glassesData.data[i] = glassesDataRight[i];
                }
                if(cl == 1){
                  glassesData.data[i] = Math.round((glassesData.data[i] + glassesDataRight[i])/2);
                }
              }
              glassesCtx.putImageData(glassesData,0,0);
              window.open(glassesCanvas.toDataURL());
            }
          }
        }
      }

      settings = new guiSettings();
      gui = new dat.GUI({autoPlace: false, width: 320});
      var editingFolder = gui.addFolder("editing");
      editingFolder.add(settings, 'pickColor').name("pick color");
      editingFolder.add(settings, 'undo');
      editingFolder.add(settings, 'brushSize', 0, 10).name("brush size").onChange(function(v){
        depthCtx.lineWidth = img.width/10*v;
      });
      brushAmountGui = editingFolder.add(settings, 'brushAmount', -2.5, 2.5).name("brush amount").onChange(function(v){
        brushS = Math.round((v+2.5)*51);
        setBrushColor();
      });
      editingFolder.add(settings, 'brushInfluence', 0, 1).name("brush influence").onChange(function(v){
        brushA = v;
        setBrushColor();
      });
      editingFolder.add(settings, 'depthAlpha',0,1).name("depth alpha").onChange(function(v){
        $("#overlayContainer").css("opacity",v);
      });
      editingFolder.add(settings, 'alwaysPickColor').name("always pick clolor");
      editingFolder.add(settings, 'uploadDepth').name("upload depth map");
      editingFolder.open();
      var cropFolder = gui.addFolder("cropping");
      cropFolder.add(settings, 'globalScale',1,10).name("scale").listen().onChange(function(v){
        linearZoom = Math.sqrt(v);
        zoomForCrop = v*v;
        setZoomAndOffset();
      });
      cropFolder.add(settings, 'globalXOffset',-1,1).name("x offset").listen().onChange(function(v){
        offsetXForCrop = v;
        setZoomAndOffset();
      });
      cropFolder.add(settings, 'globalYOffset',-1,1).name("y offset").listen().onChange(function(v){
        offsetYForCrop = v;
        setZoomAndOffset();
      });
      cropFolder.add(settings, 'xStart',0,img.width).step(1).name("crop left").listen().onChange(function(){renderBoth()});
      cropFolder.add(settings, 'xEnd',0,img.width).step(1).name("crop right").listen().onChange(function(){renderBoth()});
      cropFolder.add(settings, 'yStart',0,img.height).step(1).name("crop top").listen().onChange(function(){renderBoth()});
      cropFolder.add(settings, 'yEnd',0,img.height).step(1).name("crop bottom").listen().onChange(function(){renderBoth()});
      var renderingFolder = gui.addFolder("rendering");
      renderingFolder.add(settings, 'autoRender').name("auto render").onChange(function(v){
        if(v)
          renderBoth();
      });
      renderingFolder.add(settings, 'render');
      renderingFolder.add(settings, 'amount', -10, 10).name("depth amount").onChange(function(){renderBoth();});
      renderingFolder.add(settings, 'quality', {Full:100, Half:50, Quarter:25, "1/8":12.5, "1/16":6.25, "1/32":3.125, "1/64":1.5625, "1/128": 0.78125}).onChange(function(){renderBoth();});
      renderingFolder.add(settings, 'fixDoubleEdges').name("fix double edges").onChange(function(){renderBoth();});
      renderingFolder.add(settings, 'invertFront').name("invert front").onChange(function(){renderBoth();})
      renderingFolder.add(settings, 'exportType',{'crossview':0,'parallel view':3, 'Depth map':1,'Red&blue glasses':2,'R&B with grayscale':4}).name("export type");
      renderingFolder.add(settings, 'export').name("export");
      renderingFolder.open();
      $("#gui").html(gui.domElement);


      resize();
      bindDrawEvents();
      simpleDraw();


      depthCtx.lineWidth = img.width/10;
      depthCtx.lineCap = "round";
      depthCtx.lineJoin = "round";
      setBrushColor();
    }
    img.src = event.target.result;
  }
}

var zoomForCrop = 1, offsetXForCrop = 0, offsetYForCrop = 0;
function setZoomAndOffset(){
  var newWidth = img.width/zoomForCrop;
  var newHeight = img.height/zoomForCrop;
  var xCrop = (img.width - newWidth)/2;
  var yCrop = (img.height - newHeight)/2;
  var xOffset = xCrop*offsetXForCrop;
  var yOffset = yCrop*offsetYForCrop;

  settings.xStart = Math.round(xCrop+xOffset);
  settings.xEnd = Math.round(xCrop - xOffset);
  settings.yStart = Math.round(yCrop+yOffset);
  settings.yEnd = Math.round(yCrop - yOffset);

  renderBoth();
}

var brushS = 200, brushA = 1;
function setBrushColor(){
  depthCtx.strokeStyle = "rgba("+brushS+","+brushS+","+brushS+","+brushA+")";
}

var oldWidth=0, oldHeight=0;
function theLoop(){
  if($(window).width() != oldWidth || $(window).height() != oldHeight){
    resize();
    oldWidth = $(window).width();
    oldHeight = $(window).height();
  }

  if(mouseIsDown && points.length > 2){
    depthCtx.drawImage(depthBackup,0,0);
    depthCtx.beginPath();

    depthCtx.moveTo(toCroppedPos(points[1][0],"x"), toCroppedPos(points[1][1],"y"));
    for (i = 2; i < points.length - 1; i ++){
      depthCtx.lineTo(toCroppedPos(points[i][0],"x"), toCroppedPos(points[i][1],"y"));
    }

    // //http://stackoverflow.com/a/7058606/3625298
    // /**/  depthCtx.moveTo(toCroppedPos(points[0][0],"x"), toCroppedPos(points[0][1],"y"));
    // /**/  for (i = 1; i < points.length - 2; i ++){
    // /**/    var xc = (points[i][0] + points[i + 1][0]) / 2;
    // /**/    var yc = (points[i][1] + points[i + 1][1]) / 2;
    // /**/    xc = toCroppedPos(xc,"x");
    // /**/    yc = toCroppedPos(yc,"y");
    // /**/    depthCtx.quadraticCurveTo(toCroppedPos(points[i][0],"x"), toCroppedPos(points[i][1],"y"), xc, yc);
    // /**/  }
    // /**/  depthCtx.quadraticCurveTo(toCroppedPos(points[i][0],"x"), toCroppedPos(points[i][1],"y"), toCroppedPos(points[i+1][0],"x"), toCroppedPos(points[i+1][1],"y"));

    depthCtx.stroke();
  }

  if(isRendering1 || isRendering2){
    renderBoth(false,true);
  }

  window.requestAnimationFrame(theLoop);
}

function toCroppedPos(pos,xOrY){
  if(xOrY == "x"){
    return pos+settings.xStart;
  }else{
    return pos+settings.yStart;
  }
}

function simpleDraw(){
  simpleDrawCanvas(ctx);
  simpleDrawCanvas(ctx2);
  resize();
}

function simpleDrawCanvas(ctx){
  ctx.canvas.width=img.width-settings.xStart-settings.xEnd;
  ctx.canvas.height=img.height-settings.yStart-settings.yEnd;
  ctx.drawImage(img,-settings.xStart,-settings.yStart)
}

function setDepthVisibility(show){
  if(show){
    simpleDraw();
    $("#overlayContainer").show();
  }else{
    $("#overlayContainer").hide();
  }
}

var isRendering1 = false, isRendering2 = false, isRendered = false, exportAfterRender = false;
function renderBoth(force,isLoopRender){
  if(force || settings.autoRender || ((isRendering1 || isRendering2) && isLoopRender)){
    if(!isLoopRender){
      nextYIteration1 = null;
      nextYIteration2 = null;
      isRendering1 = true;
      isRendering2 = true;
    }
    setDepthVisibility(false);
    var invF = settings.invertFront;
    if(settings.amount < 0)
      invF = !invF;
    if(isRendering1){
      render(ctx,{
        offset: -0.0001*settings.amount*img.width,
        invertFront: !invF,
      },1);
    }
    if(isRendering2){
      render(ctx2,{
        offset: 0.0001*settings.amount*img.width,
        invertFront: invF,
      },2);
    }
    isRendering1 = nextYIteration1 != null;
    isRendering2 = nextYIteration2 != null;
    if(!isRendering1 && !isRendering2){
      isRendered = true;
      if(exportAfterRender)
        settings.export();
    }
  }else{
    isRendered = false;
    isRendering1 = false;
    isRendering2 = false;
    simpleDraw();
  }
}

var cScale;
function resize(){
  var w = $(window).width();
  var h = $(window).height();
  if(c && c2){
    cScale = (w/2)/c.width;
    $([c,c2,depthC]).css("transform","scale("+cScale+")");
    // $(depthC).css("transform","scale("+(w/2)/depthC.width+")");
    $(depthC).css({
      left: -settings.xStart*cScale,
      top: -settings.yStart*cScale,
    });
    $("#leftContainer, #rightContainer, #overlayContainer").height(c.height*cScale);
    $("#canvasContainers").css({
      width: w,
      height: c.height*cScale,
    });
  }
}

var mouseIsDown, points = [], touchPrevScale, isGesturing = false, prevGesturePos = null, isHoldingMiddleMouse = false;
function bindDrawEvents(){
  var el = $(c);


  /*mouse (draw)*/
  el.on("mousedown",function(e){
    if(e.button == 1 || e.button == 2){
      isHoldingMiddleMouse = true;
      prevGesturePos = [e.offsetX,e.offsetY];
      e.preventDefault();
    }else{
      points = [[e.offsetX,e.offsetY]];
      startBrush();
    }
  });
  el.on("mouseup mouseout",function(e){
    if(e.button == 1 || e.button == 2){
      isHoldingMiddleMouse = false;
      e.preventDefault();
    }else{
      releaseBrush();
    }
  });
  el.on("mousemove",function(e){
    if(isHoldingMiddleMouse){
      var deltaX = e.offsetX - prevGesturePos[0];
      var deltaY = e.offsetY - prevGesturePos[1];
      prevGesturePos[0] = e.offsetX;
      prevGesturePos[1] = e.offsetY;
      move(deltaX,deltaY);
    }else{
      points.push([e.offsetX,e.offsetY]);
      e.preventDefault();
    }
  });
  c.oncontextmenu = function(){
    return false;
  }





  /*touch (draw)*/
  el.on("touchstart",function(e){
    if(!isGesturing){
      points = [[e.originalEvent.pageX/cScale,e.originalEvent.pageY/cScale]];
      startBrush();
      e.preventDefault();
    }
  });
  el.on("touchend touchcancel touchleave",function(e){
    if(!isGesturing){
      releaseBrush();
      e.preventDefault();
    }
  });
  el.on("touchmove",function(e){
    if(!isGesturing){
      points.push([e.originalEvent.pageX/cScale,e.originalEvent.pageY/cScale]);
      e.preventDefault();
    }
  });


  /*mouse (move)*/
  $(document).on("mousewheel DOMMouseScroll",function(e){
    if(el.is(":hover")){
      var scroll = e.originalEvent.wheelDelta || e.originalEvent.detail;
      zoom(scroll/500);
      e.preventDefault();
    }
  });




  /*touch (move)*/
  el.on("gesturestart",function(e){
    mouseIsDown = false;
    isGesturing = true;
    prevGesturePos = [e.originalEvent.pageX,e.originalEvent.pageY];
  });

  el.on("gesturechange",function(e){
    /*scale*/
    var s = e.originalEvent.scale;
    if(touchPrevScale == null){
      var deltaScale = 0;
    }else{
      var deltaScale = s - touchPrevScale;
    }
    if(isNaN(deltaScale)){
      deltaScale = 0;
    }
    zoom(deltaScale);
    touchPrevScale = s;


    /*movement*/
    var deltaX = e.originalEvent.pageX - prevGesturePos[0];
    var deltaY = e.originalEvent.pageY - prevGesturePos[1];
    prevGesturePos[0] = e.originalEvent.pageX;
    prevGesturePos[1] = e.originalEvent.pageY;
    move(deltaX,deltaY);



    isGesturing = true;
  });

  el.on("gestureend",function(e){
    touchPrevScale = null;
    isGesturing = false;
  });
}

var linearZoom = 1;
function zoom(deltaScale){
  linearZoom += deltaScale;
  linearZoom = Math.min(100,Math.max(1,linearZoom));
  settings.globalScale = Math.pow(linearZoom -1,2)+1;
  settings.globalScale = Math.min(10,Math.max(1,settings.globalScale));
  zoomForCrop = settings.globalScale*settings.globalScale;
  setZoomAndOffset();
}

function move(deltaX,deltaY){
  settings.globalXOffset -= deltaX/50;
  settings.globalYOffset -= deltaY/50;
  settings.globalXOffset = Math.min(1,Math.max(-1,settings.globalXOffset));
  settings.globalYOffset = Math.min(1,Math.max(-1,settings.globalYOffset));
  offsetXForCrop = settings.globalXOffset;
  offsetYForCrop = settings.globalYOffset;
  setZoomAndOffset();
}

function startBrush(){
  createDepthBackup();
  setDepthVisibility(true);
  window.clearTimeout(renderTimeoutId);
  mouseIsDown = true;
  pickColorPos(toCroppedPos(points[0][0],"x"),toCroppedPos(points[0][1],"y"));
}

var isPickingColor = false;
function pickColorPos(x,y){
  if(isPickingColor || settings.alwaysPickColor){
    var r = depthCtx.getImageData(x,y,1,1).data[0];
    brushS = r;
    setBrushColor();
    brushAmountGui.setValue((r/51)-2.5);
    isPickingColor = false;
  }
}

var renderTimeoutId;
function releaseBrush(){
  if(mouseIsDown){
    mouseIsDown = false;
    renderTimeoutId = window.setTimeout(function(){
      renderBoth();
    },500);
  }
}

var depthBackup;
function createDepthBackup(){
  depthBackup = document.createElement("canvas");
  depthBackup.width = depthC.width;
  depthBackup.height = depthC.height;
  depthBackup.getContext("2d").drawImage(depthC,0,0);
}

var nextYIteration1 = null, nextYIteration2 = null;
function render(ctx,renderSettings,canvasId){
  if(typeof renderSettings === "undefined")
    renderSettings = {};
  if(typeof renderSettings.offset === "undefined")
    renderSettings.offset = 0.35;
  if(typeof renderSettings.invertFront === "undefined")
    renderSettings.invertFront = false;
  if(typeof canvasId === "undefined")
    canvasId = 1;
  var canvas = ctx.canvas;
  var depthImageData = depthCtx.getImageData(0,0,img.width,img.height).data;
  var setPixelsArr = new Array(depthImageData.length/4);
  var tmpCanvas = document.createElement("canvas");
  tmpCanvas.width = img.width;
  tmpCanvas.height = img.height;
  var tmpCtx = tmpCanvas.getContext("2d");
  var startY = 0;
  if(canvasId == 1){
    var nextYIteration = nextYIteration1;
  }else{
    var nextYIteration = nextYIteration2;
  }
  if(nextYIteration != null){
    startY = nextYIteration;
    tmpCtx.drawImage(ctx.canvas,0,0);
  }else{
    canvas.width = ((img.width - settings.xEnd) - settings.xStart)*settings.quality/100;
    canvas.height = ((img.height - settings.yEnd) - settings.yStart)*settings.quality/100;
    resize();
    tmpCtx.fillStyle = "rgba(0,0,0,255)";
    tmpCtx.fillRect(0,0,img.width,img.height);
  }
  if(canvasId == 1){
    nextYIteration1 = null;
  }else{
    nextYIteration2 = null;
  }
  var imgDataData = tmpCtx.getImageData(0,0,img.width,img.height);
  var imgData = imgDataData.data;
  var startUnix = Date.now();
  if(settings.fixDoubleEdges){
    for(var y=startY; y<canvas.height; y++){
      var croppedY = settings.yStart+(y*(100/settings.quality));
      for(var x=0; x<canvas.width; x++){
        var croppedX = settings.xStart+(x*(100/settings.quality));
        var offsetIndex = croppedX+(croppedY*img.width);
        var offset = Math.round((depthImageData[offsetIndex*4]-127)*renderSettings.offset/(100/settings.quality));
        var newX = x+offset;
        var newOffsetIndex = newX+(y*img.width);
        var isFront;
        if(renderSettings.invertFront){
          isFront = setPixelsArr[newOffsetIndex] > offset;
        }else{
          isFront = setPixelsArr[newOffsetIndex] < offset;
        }
        if(setPixelsArr[newOffsetIndex] == null || isFront){
          if(newX >= 0 && newX < img.width - settings.xEnd){
            setPixelsArr[newOffsetIndex] = offset;
            var rootIndexPos = 4*(croppedX+(croppedY*img.width));
            var newIndexPos = 4*(newX+(y*img.width));
            for(var c=0; c<4; c++){
              var rootIndex = c+rootIndexPos;
              var newIndex = c+newIndexPos;
              imgData[newIndex] = rootImgData[rootIndex];
            }
          }
        }
      }
      /*find missing pixels*/
      var lastColor = [], emptyFirstPixelsCount = 0, firstColor = null;
      for(var x=0; x<canvas.width; x++){
        var setPixelsIndex = x+(y*img.width);
        if(setPixelsArr[setPixelsIndex] == null){
          if(lastColor.length == 0){
            emptyFirstPixelsCount++;
          }else{
            for(var c=0; c<4; c++){
              var canvasColorIndex = c+(4*(x+(y*img.width)));
              imgData[canvasColorIndex] = lastColor[c];
            }
          }
        }else{
          var setFirstColor = firstColor == null;
          if(setFirstColor)
            firstColor = [];
          for(var c=0; c<4; c++){
            var canvasColorIndex = c+(4*(x+(y*img.width)));
            lastColor[c] = imgData[canvasColorIndex];
            if(setFirstColor){
              firstColor[c] = lastColor[c];
            }
          }
        }
      }
      if(firstColor != null){
        for(var x=0; x<emptyFirstPixelsCount; x++){
          for(var c=0; c<4; c++){
            var canvasColorIndex = c+(4*(x+(y*img.width)));
            imgData[canvasColorIndex] = firstColor[c];
          }
        }
      }
      if(Date.now() - 10 > startUnix){
        if(canvasId == 1){
          nextYIteration1 = y+1;
        }else{
          nextYIteration2 = y+1;
        }
        break;
      }
    }
  }else{
    for(var y=startY; y<canvas.height; y++){
      var croppedY = settings.yStart+(y*(100/settings.quality));
      for(var x=0; x<canvas.width; x++){
        var croppedX = settings.xStart+(x*(100/settings.quality));
        var offsetIndex = croppedX+(croppedY*img.width);
        var offset = Math.round((depthImageData[offsetIndex*4]-127)*renderSettings.offset/(100/settings.quality));
        var newX = croppedX-(offset*(100/settings.quality));
        var newOffsetIndex = newX+(y*img.width);
        if(newX >= 0 && newX < img.width - settings.xEnd){
          var rootIndexPos = 4*(x+(y*img.width));
          var newIndexPos = 4*(newX+(croppedY*img.width));
          for(var c=0; c<4; c++){
            var rootIndex = c+rootIndexPos;
            var newIndex = c+newIndexPos;
            imgData[rootIndex] = rootImgData[newIndex];
          }
        }
      }
      if(Date.now() - 10 > startUnix){
        if(canvasId == 1){
          nextYIteration1 = y+1;
        }else{
          nextYIteration2 = y+1;
        }
        break;
      }
    }
  }
  ctx.putImageData(imgDataData,0,0);
}

function fillMissingPixels(ctx,pixelsArray,y,x1,x2,color){
  for(var x=x1; x<=x2; x++){
    for(var c=0; c<4; c++){
      var index = c+(4*(x+(y*img.width)));
      pixelsArray[index] = color[c];
    }
  }
}

function grayScale(data){
  for(var i=0; i<data.length; i+=4){
    var r=data[i]*0.3;
    var g=data[i+1]*0.59;
    var b=data[i+2]*0.11;
    var avg = r+g+b;
    data[i] = data[i+1] = data[i+2] = avg;
  }
}