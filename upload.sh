#!/bin/sh

./build.sh

cd `dirname $0`

cp -r production/ 3d/

echo uploading production folder to jespertheend.com

ssh root@jespertheend.com <<'ENDSSH'
cd /var/www/
rm -r 3d
ENDSSH

sftp root@jespertheend.com <<**
cd /var/www/
mkdir 3d/
put -r 3d/
bye
**

rm -r 3d/
