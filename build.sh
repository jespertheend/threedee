#!/bin/sh

srcFiles="js/*.js"

debug=false
while test $# -gt 0
do
	case "$1" in
		--debug) debug=true
			;;
	esac
	shift
done

echo "building threedee"
echo "debug=$debug"

cd `dirname $0`

rm production/js.js
rm production/js.js.map
rm -r production/js/


if $debug ; then
	ujsdebugstr="--source-map url=js.js.map --beautify"
	./setTimestampVersion.sh
else
	ujsdebugstr=" --wrap Threedee --compress --mangle safari10=true"
	./setTimestampVersion.sh set
fi

npx uglifyjs $srcFiles -o production/js.js $ujsdebugstr --warn --verbose

if $debug ; then
	cp -a js/. production/js/
fi

echo "done building threedee"
