class Util{
	constructor(){}

	//returns int between min and max, min is exclusive (returns value between min and max - 1)
	static randInt(min, max) {
		return Math.floor(this.randRange(min, max));
	}

	static randRange(min, max){
		return Math.random() * (max - min) + min;
	}

	static mod(n, m) {
		return ((n % m) + m) % m;
	}

	static lerp(a,b,t){
		return a + t * (b-a);
	}

	//inverse lerp
	static iLerp(a,b,t){
		return (t - a) / (b - a);
	}

	//clamp
	static clamp(v, min, max){
		return Math.max(min, Math.min(max, v));
	}

	static clamp01(v){
		return Util.clamp(v, 0, 1);
	}
}
