class RenderViewer{
	constructor({
		isDepthViewer = false,
		right = false,
	} = {}){
		this.right = right;

		this.viewerCanvas = document.createElement("canvas");
		this.viewerCanvas.classList.add("viewerCanvas");
		this.viewerCanvas.classList.add(right ? "right" : "left");
		if(isDepthViewer) this.viewerCanvas.classList.add("depthViewer");
		this.vCtx = this.viewerCanvas.getContext("2d");
		CanvasWarningManager.testContext(this.vCtx);
		document.body.appendChild(this.viewerCanvas);
		this.isDepthViewer = isDepthViewer;
		if(isDepthViewer){
			this.renderCanvas = null;
			this.rCtx = null;
		}else{
			this.renderCanvas = document.createElement("canvas");
			this.rCtx = this.renderCanvas.getContext("2d");
			CanvasWarningManager.testContext(this.rCtx);
		}

	}

	onResize(){
		this.viewerCanvas.width = window.innerWidth / 2;
		this.viewerCanvas.height = window.innerHeight;
		this.renderViewer();
	}

	setFullImage(img){
		if(this.isDepthViewer) return;
		this.renderCanvas.width = img.width;
		this.renderCanvas.height = img.height;
		this.rCtx.drawImage(img, 0, 0);
	}

	clearFullImage(){
		if(this.isDepthViewer) return;
		this.rCtx.clearRect(0, 0, this.renderCanvas.width, this.renderCanvas.height);
	}

	clearRenderArea(x,y,w,h){
		this.rCtx.fillStyle = "black";
		this.rCtx.fillRect(x,y,w,h);
	}

	updateRenderedData(imageData,x,y,paddingLeft,paddingRight){
		if(this.isDepthViewer) return;
		this.rCtx.putImageData(imageData, x, y, paddingLeft, 0, imageData.width - paddingLeft - paddingRight, imageData.height);
		this.renderViewer();
	}

	renderViewer(){
		this.vCtx.clearRect(0, 0, this.viewerCanvas.width, this.viewerCanvas.height);
		let pos = g_main.viewerManager.viewportOffset;
		let zoom = g_main.viewerManager.viewportZoom;
		this.vCtx.imageSmoothingEnabled = zoom < 4;
		let src, w, h;
		if(this.isDepthViewer){
			src = g_main.drawingManager.isBrushing ? g_main.drawingManager.lowResCanvas : g_main.drawingManager.canvas;
			w = g_main.drawingManager.canvas.width;
			h = g_main.drawingManager.canvas.height;
		}else{
			src = this.renderCanvas;
			if(g_main.viewerManager.img && g_main.ui.view2dButton.toggleValue) src = g_main.viewerManager.img;
			w = src.width;
			h = src.height;
		}
		let depthOffset = 0;
		if(!this.isDepthViewer && (!g_main.ui.view2dButton || !g_main.ui.view2dButton.toggleValue)){
			depthOffset = g_main.viewerManager.getViewerDepthOffset(this.right ? "right" : "left");
		}
		this.vCtx.drawImage(src, pos[0] + depthOffset, pos[1], w * zoom, h * zoom);
	}

	setOpacity(opacity){
		this.viewerCanvas.style.opacity = opacity;
	}
}
