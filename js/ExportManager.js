class ExportManager{
	constructor(){
		this.imageContainer = document.getElementById("exportedImage");
		this.imageContainer.style.display = "none";
		this.imageContainer.addEventListener("click", e => {
			if(e.target == this.imageContainer){
				this.imageContainer.style.display = "none";
				this.img.src = null;
				this.downloadButton.href = null;
			}
		});

		let imageWrapper = document.createElement("div");
		imageWrapper.classList.add("exportedImageWrapper");
		this.imageContainer.appendChild(imageWrapper);

		this.img = document.createElement("img");
		this.img.classList.add("exportedImage");
		imageWrapper.appendChild(this.img);

		this.downloadButton = document.createElement("a");
		this.downloadButton.classList.add("exportImageDownloadButton");
		this.downloadButton.download = "image";
		imageWrapper.appendChild(this.downloadButton);
	}

	showExportedUrl(url){
		this.img.src = url;
		this.downloadButton.href = url;
		this.imageContainer.style.display = null;
	}

	exportCrossView(){
		this.exportTwoViews(true);
	}

	exportParallelView(){
		this.exportTwoViews(false);
	}

	async exportTwoViews(leftLeft){
		let canvas = document.createElement("canvas");
		let ctx = canvas.getContext("2d");
		if(CanvasWarningManager.testContext(ctx)) return;
		let vm = g_main.viewerManager;
		let desiredWidth = vm.img.width*2;
		let desiredHeight = vm.img.height;
		let sizeMultiplier = 1;
		let sizeMultiplierMultiplier = 0.9;
		let prevValidSize = false;
		let tries = 0;
		let hasHadValidSize = false;
		while(true){
			canvas.width = desiredWidth * sizeMultiplier;
			canvas.height = desiredHeight * sizeMultiplier;
			ctx.fillStyle = "white";
			ctx.fillRect(0,0,1,1);
			let data = ctx.getImageData(0,0,1,1).data;
			let validSize = data[0] != 0;
			if(validSize && tries == 0) break;
			hasHadValidSize = hasHadValidSize || validSize;
			if(!hasHadValidSize && tries > 10){
				this.cancelWindow();
				return false;
			}
			if(prevValidSize != validSize && validSize){
				sizeMultiplierMultiplier = Util.lerp(sizeMultiplierMultiplier, 1, 0.9);
			}
			prevValidSize = validSize;
			if(validSize){
				sizeMultiplier /= sizeMultiplierMultiplier;
			}else{
				sizeMultiplier *= sizeMultiplierMultiplier;
			}
			tries++;
			if(tries > 30 && validSize) break;
		}
		let leftViewer = leftLeft ? vm.leftViewer : vm.rightViewer;
		let rightViewer = leftLeft ? vm.rightViewer : vm.leftViewer;

		ctx.drawImage(leftViewer.renderCanvas,0,0,canvas.width/2, canvas.height);
		ctx.drawImage(rightViewer.renderCanvas,canvas.width/2,0, canvas.width/2, canvas.height);
		await this.exportCanvas(canvas);
		canvas.width = canvas.height = 0;
	}

	async exportDepthView(){
		await this.exportCanvas(g_main.drawingManager.canvas);
	}

	async exportCanvas(canvas){
		let blob = await this.canvasToBlob(canvas);
		let url = URL.createObjectURL(blob);
		this.showExportedUrl(url);
	}

	async canvasToBlob(canvas){
		return await new Promise(resolve => {
			canvas.toBlob(resolve);
		});
	}
}
