class BrushControls{
	constructor(){
		this.prevMousePos = [0,0];
		this.lastMousePos = [0,0];
		this.brushChangeMouseStartPos = [0,0];
		this.prevWasChangingBrushSettings = false;

		this.leftBrushCursor = new BrushCursor();
		this.rightBrushCursor = new BrushCursor();
		this.setCursorVisibility(false);

		this.touchListener = document.createElement("div");
		this.touchListener.classList.add("touchListener");
		this.touchListener.classList.add("fullscreen");
		this.touchListener.style.display = "none";
		document.body.appendChild(this.touchListener);

		window.addEventListener("mousemove", e => {
			this.lastMousePos = [e.pageX, e.pageY];
			let modifierKeyDown = e.ctrlKey || e.metaKey;
			if(modifierKeyDown != this.modifierKeyDown){
				this.modifierKeyDown = modifierKeyDown;
				this.updateCursor();
			}
			if(e.altKey != this.altKeyDown){
				this.altKeyDown = e.altKey;
				this.updateCursorSoftnessVisibility();
				this.updateCursor();
			}
			this.update();
			let isChangingBrushSettings = this.altKeyDown && this.mouseIsDown;
			if(this.prevWasChangingBrushSettings != isChangingBrushSettings){
				this.prevWasChangingBrushSettings = isChangingBrushSettings;
				if(isChangingBrushSettings){
					this.brushChangeMouseStartPos = [...this.lastMousePos];
				}
			}
			if(isChangingBrushSettings){
				this.setCursorPos(...this.brushChangeMouseStartPos);
			}else{
				this.setCursorPos(...this.lastMousePos);
			}
			this.setCursorVisibility(e.target == this.touchListener);
		});

		this.mouseIsDown = false;
		this.touchListener.addEventListener("mousedown", _ => {
			this.mouseIsDown = true;
			this.update();
			this.updateCursor();
		});
		this.touchListener.addEventListener("mouseup", _ => {
			this.mouseIsDown = false;
			this.update();
			this.updateCursor();
		});

		this.modifierKeyDown = false;
		this.altKeyDown = false;
		this.isUpdatingBrushSettings = false;
		window.addEventListener("keydown", e => {
			if(e.key == "Meta" || e.key == "Control"){
				this.modifierKeyDown = true;
				this.updateCursor();
			}
			if(e.key == "Alt"){
				this.altKeyDown = true;
				this.updateCursorSoftnessVisibility();
				this.updateCursor();
			}
		});
		window.addEventListener("keyup", e => {
			if(e.key == "Meta" || e.key == "Control"){
				this.modifierKeyDown = false;
				this.updateCursor();
			}
			if(e.key == "Alt"){
				this.altKeyDown = false;
				this.updateCursorSoftnessVisibility();
				this.updateCursor();
			}
		});
		window.addEventListener("blur", _ => {
			this.modifierKeyDown = false;
		});

		window.addEventListener("wheel", e => {
			e.preventDefault();
			if(e.ctrlKey || e.metaKey){
				g_main.viewerManager.zoomViewers(e.deltaY, this.lastMousePos[0], this.lastMousePos[1]);
			}else if(e.altKey){
				this.changeBrushSettings(e.deltaX, e.deltaY);
			}else{
				g_main.viewerManager.panViewers(-e.deltaX, -e.deltaY);
			}
		}, {passive: false});

		this.touchListener.addEventListener("touchstart", e => {
			e.preventDefault();
			this.updateTouches(e.touches);
		}, {passive: false});
		this.touchListener.addEventListener("touchmove", e => {
			e.preventDefault();
			this.updateTouches(e.touches);
		}, {passive: false});
		this.touchListener.addEventListener("touchend", e => {
			e.preventDefault();
			this.updateTouches(e.touches);
		}, {passive: false});
		this.prevTouches = [];
		this.prevTouchPanningCenterDistValid = false;
		this.prevTouchPanningCenterDist = 0;
		this.currentPanningTouchLeft = null;
		this.currentPanningTouchRight = null;

		this.prevWasMonitoringMouse = false;
		this.prevWasBrushing = false;

		this.forceBrushLeftRight = null;
		this.touchBrushStartTime = 0;
		this.isTouchBrushing = false;
	}

	get shouldApplyBrushOffset(){
		return !g_main.ui.view2dButton.toggleValue && g_main.ui.depthOpacityButton.sliderValue < 0.6;
	}

	update(){
		let isPanning = this.mouseIsDown && this.modifierKeyDown;
		let isBrushing = false;
		let isBrushChanging = this.altKeyDown && this.mouseIsDown;
		let isMonitoringMouse = isPanning || isBrushChanging;
		if(isMonitoringMouse){
			if(this.prevWasMonitoringMouse){
				let deltaX = this.lastMousePos[0] - this.prevMousePos[0];
				let deltaY = this.lastMousePos[1] - this.prevMousePos[1];
				if(isPanning){
					g_main.viewerManager.panViewers(deltaX, deltaY);
				}else if(isBrushChanging){
					this.changeBrushSettings(-deltaX, -deltaY);
				}
			}
			this.prevMousePos = this.lastMousePos;
		}else if(this.mouseIsDown){
			isBrushing = true;
		}
		if(this.prevWasBrushing != isBrushing){
			if(isBrushing){
				this.doPickColorThings(this.lastMousePos[0], this.lastMousePos[1]);
				g_main.drawingManager.startBrush();
			}else{
				g_main.drawingManager.endStroke();
				this.forceBrushLeftRight = null;
			}
		}
		if(isBrushing){
			this.updateBrushPos(this.lastMousePos);
		}
		this.prevWasBrushing = isBrushing;
		this.prevWasMonitoringMouse = isMonitoringMouse;
	}

	updateTouches(touches){
		let sameTouches = [];
		for(const touchA of touches){
			for(const touchB of this.prevTouches){
				if(touchA.identifier == touchB.identifier){
					sameTouches.push([touchA, touchB]);
				}
			}
		}
		if(sameTouches.length == 2 && !this.isTouchBrushing){ //panning/zooming
			if(!this.currentPanningTouchLeft || !this.currentPanningTouchRight){
				let touchA = sameTouches[0][0];
				let touchB = sameTouches[1][0];
				if(touchA.pageX > touchB.pageX){
					[touchA, touchB] = [touchB, touchA];
				}
				let touchAIsLeft = touchA.pageX < window.innerWidth / 2;
				let touchBIsLeft = touchB.pageX < window.innerWidth / 2;
				if(touchAIsLeft && !touchBIsLeft){
					let modPageX = touchB.pageX - (window.innerWidth / 2);
					if(modPageX < touchA.pageX){
						let avg = (touchA.pageX + touchB.pageX) / 2;
						touchAIsLeft = touchBIsLeft = (avg < window.innerWidth / 2);
					}
				}
				this.currentPanningTouchLeft = {
					identifier: touchA.identifier,
					isLeft: touchAIsLeft,
				}
				this.currentPanningTouchRight = {
					identifier: touchB.identifier,
					isLeft: touchBIsLeft,
				}
			}
			let highestDelta = [0,0];
			let avgPos = [0,0];
			let highestDeltaLength = 0;
			let highestCenterDist = 0;
			for(const touch of sameTouches){
				avgPos[0] += this.getPageX(touch[0]);
				avgPos[1] += touch[0].pageY;
			}
			avgPos[0] /= sameTouches.length;
			avgPos[1] /= sameTouches.length;
			for(const touch of sameTouches){
				let touchA = touch[0];
				let touchB = touch[1];
				let pageXA = this.getPageX(touchA);
				let pageXB = this.getPageX(touchB);

				let deltaX = pageXA - pageXB;
				let deltaY = touchA.pageY - touchB.pageY;
				let centerDeltaX = pageXA - avgPos[0];
				let centerDeltaY = touchA.pageY - avgPos[1];
				let centerDist = Math.sqrt(Math.pow(centerDeltaX, 2) + Math.pow(centerDeltaY, 2));
				let length = Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
				if(length > highestDeltaLength){
					highestDeltaLength = length;
					highestDelta = [deltaX, deltaY];
				}
				highestCenterDist = Math.max(highestCenterDist, centerDist);
			}
			g_main.viewerManager.panViewers(highestDelta[0], highestDelta[1]);
			if(!this.prevTouchPanningCenterDistValid){
				this.prevTouchPanningCenterDistValid = true;
			}else{
				let deltaCenterDist = this.prevTouchPanningCenterDist - highestCenterDist;
				g_main.viewerManager.zoomViewers(deltaCenterDist, avgPos[0], avgPos[1], "left");
			}
			this.prevTouchPanningCenterDist = highestCenterDist;
		}else{
			this.prevTouchPanningCenterDistValid = false;
			this.currentPanningTouchLeft = this.currentPanningTouchRight = null;
		}

		//brush
		if(this.prevTouches.length != touches.length){
			if(this.prevTouches.length < 1 && touches.length == 1){
				this.doPickColorThings(touches[0].pageX, touches[0].pageY);
				g_main.drawingManager.startBrush();
				this.touchBrushStartTime = Date.now();
				this.isTouchBrushing = true;
			}
			if(this.prevTouches.length == 1 && touches.length > 1 && Date.now() - this.touchBrushStartTime < 200){
				g_main.drawingManager.cancelStroke();
				this.isTouchBrushing = false;
			}
			if(touches.length <= 0){
				g_main.drawingManager.endStroke();
				this.forceBrushLeftRight = null;
				this.isTouchBrushing = false;
			}
		}
		this.setCursorVisibility(this.isTouchBrushing);
		if(this.isTouchBrushing){
			let touch = touches[0];
			let pos = [touch.pageX, touch.pageY];
			this.setCursorPos(...pos);
			this.updateBrushPos(pos);
		}
		this.prevTouches = touches;
	}

	getPageX(touch){
		let pageX = touch.pageX;
		let isLeft = true;
		if(touch.identifier == this.currentPanningTouchLeft.identifier){
			isLeft = this.currentPanningTouchLeft.isLeft;
		}
		if(touch.identifier == this.currentPanningTouchRight.identifier){
			isLeft = this.currentPanningTouchRight.isLeft;
		}
		if(!isLeft){
			pageX -= window.innerWidth/2;
		}
		return pageX;
	}

	screenPosToRenderPosWithDepth(screenPos, returnDepthValue = false){
		if(!this.forceBrushLeftRight){
			this.forceBrushLeftRight = screenPos[0] < window.innerWidth/2 ? "left" : "right";
		}
		let renderPos = g_main.viewerManager.screenPosToRenderPos(screenPos, this.forceBrushLeftRight, this.shouldApplyBrushOffset);
		if(this.shouldApplyBrushOffset || returnDepthValue){
			let offset = g_main.drawingManager.getOffsetAtPos(renderPos[0], renderPos[1], this.forceBrushLeftRight, returnDepthValue);
			if(returnDepthValue) return offset;
			renderPos[0] += offset;
		}
		return renderPos;
	}

	pickColor(x,y){
		g_main.ui.brushValueButton.sliderValue = g_main.drawingManager.getAbsoluteColorValue(x,y);
	}

	doPickColorThings(x,y){
		if(g_main.ui.activeCategory != UIManager.Categories.COLOR_PICKER && !(g_main.ui.alwaysPickColorButton.toggleValue && g_main.ui.activeCategory == UIManager.Categories.BRUSH_ABSOLUTE)) return;

		if(this.shouldApplyBrushOffset){
			let depth = 255 - this.screenPosToRenderPosWithDepth([x,y], true);
			g_main.ui.brushValueButton.sliderValue = depth/255;
		}else{
			let renderPos = this.screenPosToRenderPosWithDepth([x,y]);
			if(g_main.ui.activeCategory == UIManager.Categories.COLOR_PICKER){
				this.pickColor(...renderPos);
				g_main.ui.setCategory(UIManager.Categories.BRUSH_ABSOLUTE);
			}else if(g_main.ui.alwaysPickColorButton.toggleValue && g_main.ui.activeCategory == UIManager.Categories.BRUSH_ABSOLUTE){
				this.pickColor(...renderPos);
			}
		}

		if(g_main.ui.activeCategory == UIManager.Categories.COLOR_PICKER){
			g_main.ui.setCategory(UIManager.Categories.BRUSH_ABSOLUTE);
		}
	}

	updateBrushPos(screenPos){
		let renderPos = this.screenPosToRenderPosWithDepth(screenPos);
		g_main.drawingManager.updateBrushPos(renderPos[0], renderPos[1], this.forceBrushLeftRight);
	}

	setCursorPos(x, y, ignoreDepth = false){
		let w = window.innerWidth/2;
		let leftRight;
		if(x < w){
			leftRight = "left";
		}else{
			leftRight = "right";
			x -= w;
		}
		let leftOffset = 0;
		let rightOffset = 0;
		if(ignoreDepth){
			leftOffset = -2;
			rightOffset = 2;
		}else{
			let renderPos = g_main.viewerManager.screenPosToRenderPos([x,y], leftRight, true, false);
			let offset = 0;
			if(this.shouldApplyBrushOffset){
				offset = g_main.drawingManager.getOffsetAtPos(renderPos[0], renderPos[1], leftRight);
				offset *= g_main.viewerManager.viewportZoom * 2;
				offset -= g_main.viewerManager.getViewerDepthOffset(leftRight) * 2;
			}
			if(leftRight == "left"){
				rightOffset = offset;
			}else{
				leftOffset = offset;
			}
		}
		this.leftBrushCursor.setPos(x+leftOffset, y);
		this.rightBrushCursor.setPos(x+w+rightOffset, y);
		this.isUpdatingBrushSettings = false;
		this.updateCursorSoftnessVisibility();
	}

	changeBrushSettings(deltaX, deltaY){
		g_main.ui.brushSizeButton.sliderValue += deltaY;
		if(g_main.ui.brushSoftnessButton.visible){
			g_main.ui.brushSoftnessButton.sliderValue -= deltaX * 0.01;
		}
		this.updateCursorSize();
		this.updateCursorSoftness();
		this.isUpdatingBrushSettings = true;
		this.updateCursorSoftnessVisibility();
	}

	updateCursorSize(){
		let size = g_main.drawingManager.brushSize*g_main.viewerManager.viewportZoom;
		this.leftBrushCursor.setSize(size);
		this.rightBrushCursor.setSize(size);
	}

	centerCursor(){
		if(!g_main.viewerManager.img) return;
		this.setCursorVisibility(true);
		this.setCursorPos(window.innerWidth/4, window.innerHeight/2, true);
		if(g_main.ui.brushSoftnessButton.visible){
			this.updateCursorSoftness();
			this.isUpdatingBrushSettings = true;
			this.updateCursorSoftnessVisibility();
		}
	}

	setCursorVisibility(visible){
		this.leftBrushCursor.setVisibility(visible);
		this.rightBrushCursor.setVisibility(visible);
	}

	updateCursorSoftness(){
		let softness = 0;
		if(g_main.ui.brushSoftnessButton.visible){
			softness = g_main.ui.brushSoftnessButton.sliderValue * g_main.drawingManager.brushSize * g_main.viewerManager.viewportZoom * 0.25;
		}
		this.leftBrushCursor.setSoftness(softness);
		this.rightBrushCursor.setSoftness(softness);
	}

	updateCursorSoftnessVisibility(){
		let visible = this.altKeyDown || this.isUpdatingBrushSettings;
		this.leftBrushCursor.setSoftnessVisibility(visible);
		this.rightBrushCursor.setSoftnessVisibility(visible);
		this.updateCursorSoftness();
		this.updateCursorSize();
	}

	updateCursor(){
		let cursor = "default";
		if(this.modifierKeyDown){
			cursor = this.mouseIsDown ? "grabbing" : "grab";
		}
		if(this.altKeyDown){
			cursor = "ns-resize";
		}
		this.touchListener.style.cursor = cursor;
	}
}
