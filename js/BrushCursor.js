class BrushCursor{
	constructor(){
		this.el = document.createElement("div");
		this.el.classList.add("brushCursor");
		document.body.appendChild(this.el);

		this.softnessEl = document.createElement("div");
		this.softnessEl.classList.add("brushSoftnessCursor");
		this.el.appendChild(this.softnessEl);

		this.setSize(100);
		this.setSoftness(0);
		this.setVisibility(false);
	}

	setSize(size){
		this.el.style.width = this.el.style.height = size+"px";
	}

	setPos(x, y){
		this.el.style.left = x+"px";
		this.el.style.top = y+"px";
	}

	setVisibility(visible){
		this.el.style.display = visible ? null : "none";
	}

	setSoftnessVisibility(visible){
		this.softnessEl.style.display = visible ? null : "none";
	}

	setSoftness(softness){
		this.softnessEl.style.filter = `blur(${softness}px)`;
	}
}
