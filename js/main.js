let VERSION_TIMESTAMP = "000";

class Main{
	constructor(){
		this.fileUploader = new FileUploader();
		this.viewerManager = new ViewerManager();
		this.workerPool = new WorkerPool();
		this.ui = new UIManager();
		this.brushControls = new BrushControls();
		this.drawingManager = new DrawingManager();
		this.exportManager = new ExportManager();
	}

	init(){
		IndexedDbUtil.init();

		this.viewerManager.init();
		this.ui.init();
		this.installSw();
	}

	async installSw(){
		if(!('serviceWorker' in window.navigator)) return;
		await navigator.serviceWorker.register('sw.js');
	}
}

//add for...of loops on associative arrays
//bedankt joris
Symbol.myDictIterator = Symbol();
Object.prototype[Symbol.myDictIterator] = function*(){
  for(let key of Object.getOwnPropertyNames(this)){
    yield [key,this[key]];
  }
}
Object.defineProperty(Object.prototype, 'keyValues', {
  get: function(){
    return this[Symbol.myDictIterator]();
  }
});

let g_main = null;
window.onload = function(){
	(g_main = new Main()).init();
}
