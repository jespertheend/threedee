class SharpenImageData{
	constructor(){}

	static sharpen(imageData){
		for(let x=0; x<imageData.width; x++){
			for(let y=0; y<imageData.height; y++){
				let index = this.getIndex(imageData, x, y);
				let currentValue = this.getValue(imageData, x, y, 2);
				let highestValue = currentValue;
				let smallestValue = currentValue;
				for(const side of [[0,1], [1,0], [-1,0], [0,-1]]){
					let sidePixelValue = this.getValue(imageData, x+side[0], y+side[1], 2);
					if(sidePixelValue != null){
						highestValue = Math.max(highestValue, sidePixelValue);
						smallestValue = Math.min(smallestValue, sidePixelValue);
					}
				}
				let smallestDist = Math.abs(currentValue - smallestValue);
				let highestDist = Math.abs(currentValue - highestValue);
				let newValue;
				if(smallestDist < highestDist){
					newValue = smallestValue;
				}else{
					newValue = highestValue;
				}
				imageData.data[index+1] = newValue;
			}
		}
	}

	static getValue(imageData, x, y, offset = 0){
		let index = this.getIndex(imageData, x, y, offset);
		if(index < 0) return null;
		return imageData.data[index];
	}

	static getIndex(imageData, x, y, offset = 0){
		if(x < 0 || x >= imageData.width || y < 0 || y >= imageData.height) return -1;
		return y*4*imageData.width + x*4 + offset;
	}
}
