class CanvasWarningManager{
	constructor(){}

	static testContext(ctx){
		if(!ctx){
			this.warn();
			return true;
		}
		return false;
	}

	static warn(){
		if(this.didWarn) return;
		this.didWarn = true;
		alert("Unable to load canvas context! Your browser might not support canvas or you are on a low end device that has run out of memory. Sometimes restarting your browser/device works if this happens.");
	}
}
