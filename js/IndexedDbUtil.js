class IndexedDbUtil{
	static get dbName(){
		return "keyValuesDb";
	}

	static get objectStoreName(){
		return "keyValues";
	}

	static init(){
		this.supported = false;
		try{
			let dbRequest = indexedDB.open(this.dbName);
			dbRequest.onupgradeneeded = (e) => {
				dbRequest.result.createObjectStore(this.objectStoreName);
			};
			this.supported = true;
		}catch(e){
			console.log("error while opening indexedDB: ",e);
		}
	}

	static get(key){
		if(!this.supported){
			let val = localStorage.getItem("indexedDBFallback"+key);
			try{
				val = JSON.parse(val);
			}catch(e){
				val = null;
			}
			return Promise.resolve(val);
		}
		return new Promise((resolve, reject) => {
			let request = indexedDB.open(this.dbName);
			request.onsuccess = (e) => {
				let db = request.result;
				let transaction = db.transaction(this.objectStoreName, "readonly");
				let objectStore = transaction.objectStore(this.objectStoreName);
				let getRequest = objectStore.get(key);
				getRequest.onsuccess = () => {
					resolve(getRequest.result);
				}
				getRequest.onerror = () => {
					reject();
				}
			}
			request.onerror = () => {
				reject();
			}
		});
	}

	static set(key,value){
		return this.getSet(key, () => {
			return value;
		});
	}

	static getSet(key, cb){
		if(!this.supported){
			let val = localStorage.getItem(key);
			let newVal = cb(val);
			localStorage.setItem("indexedDBFallback"+key, JSON.stringify(newVal));
			return Promise.resolve();
		}
		return new Promise((resolve, reject) => {
			let request = indexedDB.open(this.dbName);
			request.onsuccess = (e) => {
				let db = request.result;
				let transaction = db.transaction(this.objectStoreName, "readwrite");
				let objectStore = transaction.objectStore(this.objectStoreName);
				let cursorRequest = objectStore.openCursor(key);
				cursorRequest.onsuccess = () => {
					let cursor = cursorRequest.result;
					if(cursor){
						let newVal = cb(cursor.value);
						let cursorRequest = cursor.update(newVal);
						cursorRequest.onsuccess = () => {
							resolve();
						}
						cursorRequest.onerror = () => {
							reject();
						}
					}else{
						let putRequest = objectStore.put(cb(null), key);
						putRequest.onsuccess = () => {
							resolve();
						}
						putRequest.onerror = () => {
							reject();
						}
					}
				}
				cursorRequest.onerror = () => {
					reject();
				}
			}
			request.onerror = (e) => {
				reject();
			}
		});
	}
}