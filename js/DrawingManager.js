class DrawingManager{
	constructor(){
		this.canvas = document.createElement("canvas");
		this.ctx = this.canvas.getContext("2d");
		CanvasWarningManager.testContext(this.ctx);
		this.w = this.canvas.width;
		this.h = this.canvas.height;
		this.lowResCanvas = document.createElement("canvas");
		this.lowResCtx = this.lowResCanvas.getContext("2d");
		CanvasWarningManager.testContext(this.lowResCtx);
		this.lowW = this.lowResCanvas.width;
		this.lowH = this.lowResCanvas.height;
		this.lowResSizeMultiplier = 1;
		this.strokeCanvas = document.createElement("canvas");
		this.strokeCanvas.width = this.strokeCanvas.height = 10;
		this.strokeCtx = this.strokeCanvas.getContext("2d");
		CanvasWarningManager.testContext(this.strokeCtx);
		this.blurCanvas = document.createElement("canvas");
		this.blurCanvas.width = this.blurCanvas.height = 10;
		this.blurCtx = this.blurCanvas.getContext("2d");
		CanvasWarningManager.testContext(this.blurCtx);

		this.isBrushing = false;
		this.beforeStrokeImageData = null;
		this.currentStrokes = [];

		this.endBrushTimeout = new Timeout(_ => {
			this.endBrush();
		}, 300);
	}

	get brushSize(){
		return g_main.ui.brushSizeButton.sliderValue * this.canvas.width / 300;
	}

	setDepthImage(img){
		this.ctx.drawImage(img, 0, 0, this.w, this.h);
		//make grayscale
		let imageData = this.ctx.getImageData(0,0,this.w,this.h);
		for(let i=0; i<imageData.data.length; i+=4){
			imageData.data[i+1] = imageData.data[i+2] = imageData.data[i];
			imageData.data[i+3] = 255;
		}
		this.ctx.putImageData(imageData, 0, 0);
		this.setLowResCanvasSize();
		this.saveDepthData();
		g_main.viewerManager.fullRender();
		g_main.viewerManager.renderAllViewers();
	}

	clearTransparent(){
		this.ctx.clearRect(0,0,this.w,this.h);
	}

	clearWithSize(w,h,save = true){
		this.canvas.width = this.w = w;
		this.canvas.height = this.h = h;
		this.ctx.fillStyle = "rgb(128,128,128)";
		this.ctx.fillRect(0,0,w,h);
		this.setLowResCanvasSize();
		if(save) this.clearSavedDepthData();
	}

	setLowResCanvasSize(){
		let aspect = this.w/this.h;
		let w1 = Math.min(300, this.w);
		let h1 = w1/aspect;
		let h2 = Math.min(300, this.h);
		let w2 = h2*aspect;
		let w,h;
		if(w1 < w2){
			w = w1;
			h = h1;
		}else{
			w = w2;
			h = h2;
		}
		this.lowResCanvas.width = this.lowW = w;
		this.lowResCanvas.height = this.lowH = h;
		this.lowResSizeMultiplier = this.lowW / this.w;
	}

	startBrush(){
		this.endBrushTimeout.stop();
		g_main.workerPool.pauseRendering();
		g_main.viewerManager.cancelRenderTimeout();
		if(!this.isBrushing){
			this.isBrushing = true;
			this.beforeStrokeImageData = this.ctx.getImageData(0,0, this.w, this.h);
			this.lowResCtx.globalCompositeOperation = "source-over";
			this.lowResCtx.drawImage(this.canvas, 0, 0, this.lowW, this.lowH);
			this.beforeStrokeImageDataLowRes = this.lowResCtx.getImageData(0,0, this.lowW, this.lowH);
			this.currentStrokes = [{
				positions:[],
				absoluteSliderValue: 0,
			}];
		}
		if(this.currentStrokes.length > 0){
			let last = this.currentStrokes[this.currentStrokes.length - 1];
			last.absoluteSliderValue = g_main.ui.brushValueButton.sliderValue;
		}
	}

	endStroke(){
		if(!this.isBrushing) return;
		this.currentStrokes.push({
			positions:[],
			absoluteSliderValue: 0,
		});
		if(this.currentStrokes.length > 10){
			this.endBrush();
		}else{
			this.endBrushTimeout.start();
		}
		return;
	}

	endBrush(){
		if(!this.isBrushing) return;

		//calculate bounding box
		let minStrokePos = [Infinity,Infinity];
		let maxStrokePos = [0,0];
		let hasValue = false;
		for(const stroke of this.currentStrokes){
			for(const pos of stroke.positions){
				hasValue = true;
				minStrokePos[0] = Math.min(minStrokePos[0], pos[0]);
				minStrokePos[1] = Math.min(minStrokePos[1], pos[1]);
				maxStrokePos[0] = Math.max(maxStrokePos[0], pos[0]);
				maxStrokePos[1] = Math.max(maxStrokePos[1], pos[1]);
			}
		}
		if(!hasValue){
			this.isBrushing = false;
			return;
		}
		let padding = this.brushSize/2;
		minStrokePos[0] -= padding;
		minStrokePos[1] -= padding;
		maxStrokePos[0] += padding;
		maxStrokePos[1] += padding;

		let x = minStrokePos[0];
		let y = minStrokePos[1];
		let w = maxStrokePos[0] - minStrokePos[0];
		let h = maxStrokePos[1] - minStrokePos[1];


		this.drawCurrentStroke(true, [x,y,w,h]);
		this.isBrushing = false;
		g_main.viewerManager.renderDepthViewers();

		this.saveDepthData();


		g_main.viewerManager.renderAreasWithTimeout(x, y, w, h);
	}

	cancelStroke(){
		if(!this.isBrushing) return;
		this.currentStrokes.pop();
		this.endBrush();
		g_main.viewerManager.renderDepthViewers();
		g_main.workerPool.continueRendering();
	}

	updateBrushPos(x,y,leftRight){
		if(!this.isBrushing) return;
		let currentStroke = this.currentStrokes[this.currentStrokes.length - 1];
		currentStroke.positions.push([x,y]);
		this.drawCurrentStroke();
		g_main.viewerManager.renderDepthViewers();
	}

	getOffsetAtPos(x,y,leftRight,returnDepthValue = false){
		let imageData = this.ctx.getImageData(0,y,this.w,1).data;
		let depthAmount = g_main.viewerManager.depthAmount;
		if(leftRight == "right") depthAmount *= -1;

		let lowestDepth = Infinity;
		let bestOffset = 0;
		let startX = 0;
		let endX = this.w;
		for(let i=startX; i<endX - 1; i++){
			let depthValue1 = 255 - imageData[i*4];
			let depthValue2 = 255 - imageData[(i+1)*4];
			let originalXPos1 = i;
			let originalXPos2 = i + 1;
			let newPos1 = this.calcNewXPosForPixel(originalXPos1, depthValue1, depthAmount);
			let newPos2 = this.calcNewXPosForPixel(originalXPos2, depthValue2, depthAmount);

			if(newPos1 <= x && newPos2 >= x){
				let lerpValue = Util.iLerp(newPos1, newPos2, x);
				let depthValue = Util.lerp(depthValue1, depthValue2, lerpValue);
				if(depthValue < lowestDepth){
					lowestDepth = depthValue;
					let originalXPos = Util.lerp(originalXPos1, originalXPos2, lerpValue);
					bestOffset = originalXPos - x;
				}
			}
		}
		return returnDepthValue ? lowestDepth : bestOffset;
	}

	calcNewXPosForPixel(originalXPos, pixelDepthAmount, globalDepthAmount){
		return originalXPos + Math.round((pixelDepthAmount - 128) * globalDepthAmount);
	}

	getAbsoluteColorValue(x,y){
		x = Util.clamp(x, 0, this.w - 1);
		y = Util.clamp(y, 0, this.h - 1);
		let imageData = this.ctx.getImageData(x,y,1,1).data;
		return imageData[0]/255;
	}

	drawCurrentStroke(isEnding = false, bounds = null){
		if(!this.isBrushing) return;
		let ctx = isEnding ? this.ctx : this.lowResCtx;
		let w, h;
		if(isEnding){
			w = this.w;
			h = this.h;
		}else{
			w = this.lowW;
			h = this.lowH;
		}
		ctx.clearRect(0,0,w,h);
		ctx.putImageData(isEnding ? this.beforeStrokeImageData : this.beforeStrokeImageDataLowRes, 0, 0);


		let depthAddAmount = g_main.ui.brushAddAmountButton.sliderValue;
		let additive = g_main.ui.activeCategory == UIManager.Categories.BRUSH_ADDITIVE;
		let absolute = g_main.ui.activeCategory == UIManager.Categories.BRUSH_ABSOLUTE;
		let blur = g_main.ui.activeCategory == UIManager.Categories.BRUSH_BLUR;
		let sharpen = g_main.ui.activeCategory == UIManager.Categories.BRUSH_SHARPEN;
		let additiveInvert = depthAddAmount < 0;
		let brushOpacity = 1;
		let softness = g_main.ui.brushSoftnessButton.sliderValue;
		let brushSize = this.brushSize;
		if(!isEnding) brushSize *= this.lowResSizeMultiplier;
		if(additive){
			brushOpacity = Math.abs(depthAddAmount);
		}else if(absolute){
			brushOpacity = g_main.ui.brushOpacityButton.sliderValue;
		}else if(blur){
			softness = 0;
			if(isEnding){
				let imageData = ctx.getImageData(0, 0, w, h);
				for(let i=0; i<imageData.data.length; i+=4){
					imageData.data[i+1] = 0;
					imageData.data[i+2] = 0;
					imageData.data[i+3] = 255;
				}
				ctx.putImageData(imageData, 0, 0);
			}
		}else if(sharpen){
			softness = 0;
			if(isEnding){
				let imageData = ctx.getImageData(0, 0, w, h);
				for(let i=0; i<imageData.data.length; i+=4){
					imageData.data[i] = 0;
					imageData.data[i+1] = 0;
					imageData.data[i+3] = 255;
				}
				ctx.putImageData(imageData, 0, 0);
			}
		}



		this.strokeCanvas.width = w;
		this.strokeCanvas.height = h;
		this.strokeCtx.lineJoin = this.strokeCtx.lineCap = "round";
		for(const stroke of this.currentStrokes){
			this.strokeCtx.fillStyle = "black";
			this.strokeCtx.globalAlpha = 1;
			this.strokeCtx.fillRect(0,0,w,h);
			let softnessIterations = Util.lerp(1, brushSize*0.5, softness);

			for(let i=0; i<softnessIterations; i++){
				this.strokeCtx.lineWidth = brushSize*Util.lerp(1, 1 - i/softnessIterations, softness);
				let strokeBrightness = (i+1)/softnessIterations;
				strokeBrightness *= 255;
				strokeBrightness = Math.round(strokeBrightness);
				this.strokeCtx.strokeStyle = `rgb(${strokeBrightness},${strokeBrightness},${strokeBrightness})`;

				this.strokeCtx.beginPath();
				for(let i=0; i<stroke.positions.length; i++){
					let pos = stroke.positions[i];
					let x = pos[0];
					let y = pos[1];
					if(!isEnding){
						x *= this.lowResSizeMultiplier;
						y *= this.lowResSizeMultiplier;
					}
					if(i == 0){
						this.strokeCtx.moveTo(x, y);
					}else{
						this.strokeCtx.lineTo(x, y);
					}
				}
				this.strokeCtx.stroke();
			}
			this.strokeCtx.globalAlpha = 1 - brushOpacity;
			this.strokeCtx.fillStyle = "black";
			this.strokeCtx.fillRect(0,0,w,h);

			if(additive){
				if(additiveInvert) this.invertCanvas(ctx);
				ctx.globalCompositeOperation = "lighter";
				ctx.drawImage(this.strokeCanvas, 0, 0);
				if(additiveInvert) this.invertCanvas(ctx);
			}else if(absolute){
				let imageData = this.strokeCtx.getImageData(0, 0, w, h);
				for(let i=0; i<imageData.data.length; i+=4){
					imageData.data[i+3] = imageData.data[i];
					let absoluteValue = stroke.absoluteSliderValue * 255;
					imageData.data[i] = imageData.data[i+1] = imageData.data[i+2] = absoluteValue;
				}
				this.strokeCtx.putImageData(imageData, 0, 0);
				ctx.globalCompositeOperation = "source-over";
				ctx.drawImage(this.strokeCanvas, 0, 0);
			}else if(blur){
				let imageData = this.strokeCtx.getImageData(0, 0, w, h);
				for(let i=0; i<imageData.data.length; i+=4){
					imageData.data[i+3] = imageData.data[i];
					imageData.data[i] = 0;
					imageData.data[i+1] = 255;
					imageData.data[i+2] = 0;
				}
				this.strokeCtx.putImageData(imageData, 0, 0);
				ctx.globalCompositeOperation = isEnding ? "lighter" : "source-over";
				ctx.drawImage(this.strokeCanvas, 0, 0);
			}else if(sharpen){
				let imageData = this.strokeCtx.getImageData(0, 0, w, h);
				for(let i=0; i<imageData.data.length; i+=4){
					imageData.data[i+3] = imageData.data[i];
					imageData.data[i] = 255;
					imageData.data[i+1] = 0;
					imageData.data[i+2] = 0;
				}
				this.strokeCtx.putImageData(imageData, 0, 0);
				ctx.globalCompositeOperation = isEnding ? "lighter" : "source-over";
				ctx.drawImage(this.strokeCanvas, 0, 0);
			}
		}

		this.strokeCanvas.width = this.strokeCanvas.height = 10;

		if(isEnding){
			if(blur){
				this.blurCanvas.width = w;
				this.blurCanvas.height = h;
				this.blurCtx.drawImage(this.canvas, 0, 0);

				let radius = g_main.ui.brushBlurAmountButton.sliderValue * w / 50;

				let imageData = this.blurCtx.getImageData(0, 0, w, h);
				stackBoxBlurCanvasRGB(imageData, 0, 0, w, h, radius, 2);
				for(let i=0; i<imageData.data.length; i+=4){
					imageData.data[i+3] = imageData.data[i+1];
					imageData.data[i+1] = imageData.data[i+2] = imageData.data[i];
				}
				this.blurCtx.putImageData(imageData, 0, 0);

				this.ctx.globalCompositeOperation = "source-over";
				this.ctx.clearRect(0,0,w,h);
				this.ctx.putImageData(this.beforeStrokeImageData, 0, 0);
				this.ctx.drawImage(this.blurCanvas, 0, 0);
				this.blurCanvas.width = this.blurCanvas.height = 0;
			}else if(sharpen){
				let boundsX = bounds[0];
				let boundsY = bounds[1];
				let boundsW = bounds[2];
				let boundsH = bounds[3];

				this.blurCanvas.width = boundsW;
				this.blurCanvas.height = boundsH;
				this.blurCtx.drawImage(this.canvas, boundsX, boundsY, boundsW, boundsH, 0, 0, boundsW, boundsH);

				let imageData = this.blurCtx.getImageData(0, 0, boundsW, boundsH);
				SharpenImageData.sharpen(imageData);
				for(let i=0; i<imageData.data.length; i+=4){
					imageData.data[i+3] = imageData.data[i];
					imageData.data[i] = imageData.data[i+2] = imageData.data[i+1];
				}
				this.blurCtx.putImageData(imageData, 0, 0);

				this.ctx.globalCompositeOperation = "source-over";
				this.ctx.clearRect(0,0,w,h);
				this.ctx.putImageData(this.beforeStrokeImageData, 0, 0);
				this.ctx.drawImage(this.blurCanvas, boundsX, boundsY);
				this.blurCanvas.width = this.blurCanvas.height = 0;
			}
		}
	}

	invertCanvas(ctx){
		ctx.globalCompositeOperation = "difference";
		ctx.fillStyle = "white";
		ctx.fillRect(0,0,this.ctx.canvas.width,this.ctx.canvas.height);
	}

	invertDepthMap(){
		this.invertCanvas(this.ctx);
		this.saveDepthData();
		g_main.viewerManager.fullRender();
		g_main.viewerManager.renderAllViewers();
	}

	saveDepthData(){
		let data = this.ctx.getImageData(0,0,this.w, this.h);
		IndexedDbUtil.set("lastDepthImage", data);
	}

	clearSavedDepthData(){
		IndexedDbUtil.set("lastDepthImage", null);
	}
}
