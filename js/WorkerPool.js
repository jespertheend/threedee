class WorkerPool{
	constructor(){
		this.workers = [];
		this.lastRenderId = 0;
		this.activeRenders = [];
		this.renderingPaused = false;

		let threads = navigator.hardwareConcurrency || 4;
		threads--;
		threads = Math.max(1, threads);
		for(let i=0; i < threads; i++){
			let worker = new Worker("3dRenderWorker.js?v="+VERSION_TIMESTAMP);
			worker.onmessage = e => {
				this.onWorkerMessage(e.data, i);
			}
			this.workers.push({
				worker: worker,
				queueLength: 0,
			});
		}
	}

	renderArea(rgbData, depthData, w, h, depthAmount){
		let bestWorkerIndex = -1;
		let bestWorkerQueue = Infinity;
		for(let i=this.workers.length - 1; i >= 0; i--){
			let queueLength = this.workers[i].queueLength;
			if(queueLength < bestWorkerQueue){
				bestWorkerIndex = i;
				bestWorkerQueue = queueLength;
			}
		}
		let bestWorker = this.workers[bestWorkerIndex];
		bestWorker.queueLength++;

		this.lastRenderId++;

		bestWorker.worker.postMessage({
			opCode: 0,
			rgbData: rgbData,
			depthData: depthData,
			renderId: this.lastRenderId,
			width: w,
			height: h,
			depthAmount: depthAmount,
		}, [rgbData, depthData]);

		this.activeRenders.push({
			id: this.lastRenderId,
			workerIndex: bestWorkerIndex,
			cbs: [],
			hasBeenCancelled: false,
		});

		return this.lastRenderId;
	}

	getRenderById(id){
		for(const render of this.activeRenders){
			if(render.id == id){
				return render;
			}
		}
		return null;
	}

	async waitForRenderId(id){
		let render = this.getRenderById(id);
		if(!render) return null;
		return await new Promise((resolve, reject) => {
			render.cbs.push(resolve);
		});
	}

	onWorkerMessage(data, workerIndex){
		this.workers[workerIndex].queueLength--;
		let renderId = data.renderId;
		for(let i=this.activeRenders.length -1; i >= 0; i--){
			let render = this.activeRenders[i];
			if(render.id == renderId){
				let rgbData = render.hasBeenCancelled ? null : data.rgbData;
				for(const cb of render.cbs){
					cb(rgbData);
				}
				this.activeRenders.splice(i, 1);
				break;
			}
		}
	}

	clearAllCurrentRenders(){
		this.activeRenders = [];
		for(const worker of this.workers){
			worker.queueLength = 0;
			worker.worker.postMessage({
				opCode: 1,
			});
		}
	}

	pauseRendering(){
		this.renderingPaused = true;
		for(const worker of this.workers){
			worker.worker.postMessage({
				opCode: 2,
			});
		}
	}

	continueRendering(){
		this.renderingPaused = false;
		for(const worker of this.workers){
			worker.worker.postMessage({
				opCode: 3,
			});
		}
	}

	cancelRenderIds(ids){
		for(const id of ids){
			let render = this.activeRenders.find(r => r.id == id);
			if(render && !render.hasBeenCancelled){
				render.hasBeenCancelled = true;
				this.workers[render.workerIndex].worker.postMessage({
					opCode: 4,
					id: id,
				});
			}
		}
	}

	setRenderSettings(settings){
		for(const worker of this.workers){
			worker.worker.postMessage({
				opCode: 5,
				settings: settings,
			});
		}
	}
}
