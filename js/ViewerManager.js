class ViewerManager{
	constructor(){
		this.img = null;
		this.rawCanvas = document.createElement("canvas");
		this.rawCtx = this.rawCanvas.getContext("2d");
		CanvasWarningManager.testContext(this.rawCtx);
		this.leftViewer = new RenderViewer();
		this.rightViewer = new RenderViewer({
			right: true,
		});

		this.viewportOffset = [0,0];
		this.viewportZoomRaw = 1;

		window.addEventListener("resize", _ => {
			this.onResize();
		});
		this.prevW = window.innerWidth;
		this.prevH = window.innerHeight;
		window.setInterval(_ => {
			if(window.innerWidth != this.prevW || window.innerHeight != this.prevH){
				this.prevW = window.innerWidth;
				this.prevH = window.innerHeight;
				this.onResize();
			}
		}, 300);

		this.renderWhenReadyArea = null;
		this.renderWhenReadyTimeout = new Timeout(_ => {
			if(this.renderWhenReadyArea){
				if(!this.renderWhenReadyArea) return;
				let x = Math.max(0, Math.min(this.rawCanvas.width, this.renderWhenReadyArea[0]));
				let y = Math.max(0, Math.min(this.rawCanvas.height, this.renderWhenReadyArea[1]));
				let x2 = Math.max(0, Math.min(this.rawCanvas.width, this.renderWhenReadyArea[2]));
				let y2 = Math.max(0, Math.min(this.rawCanvas.height, this.renderWhenReadyArea[3]));
				let w = x2 - x;
				let h = y2 - y;

				[x,y,w,h] = this.cancelOverlappingRenderIds(x,y,w,h);
				this.renderWhenReadyArea[0] = x;
				this.renderWhenReadyArea[1] = y;
				this.renderWhenReadyArea[2] = x + w;
				this.renderWhenReadyArea[3] = y + h;

				if(g_main.workerPool.activeRenders.length > 10){
					this.renderWhenReadyTimeout.start(); //retry in 500ms
				}else{
					this.renderWhenReadyArea = null;
					this.renderAreas(x,y,w,h);
				}
			}
			if(!g_main.drawingManager.isBrushing) g_main.workerPool.continueRendering();
		}, 500);

		this.renderingAreas = [];
	}

	get depthAmount(){
		let imgW = 100;
		if(this.img) imgW = this.img.width;
		return 0.0001*g_main.ui.settingsDepthAmountButton.sliderValue*imgW;
	}

	getViewerDepthOffset(leftRight){
		let depthOffset = 0;
		if(g_main.ui.settingsDepthAmountButton){
			depthOffset = this.depthAmount * this.viewportZoom * 130;
		}
		if(leftRight == "right") depthOffset *= -1;
		return depthOffset;
	}

	init(){
		this.leftDepthViewer = new RenderViewer({
			isDepthViewer: true,
		});
		this.rightDepthViewer = new RenderViewer({
			isDepthViewer: true,
			right: true,
		});
		this.onResize();
		this.loadLastImage();
		this.loadViewport();
	}

	saveViewport(){
		IndexedDbUtil.set("viewportOffset", this.viewportOffset);
		IndexedDbUtil.set("viewportZoomRaw", this.viewportZoomRaw);
	}

	async loadViewport(){
		let viewportOffset = await IndexedDbUtil.get("viewportOffset");
		if(viewportOffset !== undefined) this.viewportOffset = viewportOffset;
		let viewportZoomRaw = await IndexedDbUtil.get("viewportZoomRaw");
		if(viewportZoomRaw !== undefined) this.viewportZoomRaw = viewportZoomRaw;
		g_main.brushControls.updateCursorSize();
		this.renderAllViewers();
	}

	get viewportZoom(){
		return Math.pow(this.viewportZoomRaw, 2);
	}

	onResize(){
		this.leftViewer.onResize();
		this.rightViewer.onResize();
		this.leftDepthViewer.onResize();
		this.rightDepthViewer.onResize();
	}

	setFullImage(img, save = true, testSuccess = true){
		this.img = img;
		this.rawCanvas.width = img.width;
		this.rawCanvas.height = img.height;
		this.rawCtx.drawImage(img, 0, 0);
		if(testSuccess && this.rawCanvas.toDataURL().length < 10){
			alert("failed to load image (it might be too big)");
			this.img = null;
			return;
		}
		if(save){
			let data = this.rawCtx.getImageData(0,0,img.width, img.height);
			IndexedDbUtil.set("lastRgbImage", data);
		}

		this.leftViewer.setFullImage(img);
		this.rightViewer.setFullImage(img);
		g_main.drawingManager.clearWithSize(img.width, img.height, save);
		if(save) this.centerViewport();
	}

	async clearFullImage(){
		await IndexedDbUtil.set("lastRgbImage", null);
		await IndexedDbUtil.set("lastDepthImage", null);
		this.img = null;
		g_main.fileUploader.setLoadedImageEls();
		this.leftViewer.clearFullImage();
		this.rightViewer.clearFullImage();
		g_main.drawingManager.clearTransparent();
		this.renderAllViewers();
	}

	async loadLastImage(){
		let data = await IndexedDbUtil.get("lastRgbImage");
		if(data){
			let canvas = document.createElement("canvas");
			canvas.width = data.width;
			canvas.height = data.height;
			let ctx = canvas.getContext("2d");
			if(CanvasWarningManager.testContext(ctx)) return;
			ctx.putImageData(data, 0, 0);
			let rgbImg;
			try{
				rgbImg = await this.getImageAsync(canvas.toDataURL());
			}catch(_){
				alert("failed to load the image from the previous session (it might be too big)");
				await this.clearFullImage();
				return;
			}
			this.setFullImage(rgbImg, false, false);
			g_main.fileUploader.setLoadedImageEls();

			let depthData = await IndexedDbUtil.get("lastDepthImage");
			if(depthData && depthData.width == canvas.width && depthData.height == canvas.height){
				ctx.putImageData(depthData, 0, 0);
				let depthImg;
				try{
					depthImg = await this.getImageAsync(canvas.toDataURL());
				}catch(_){
					alert("failed to load the depth map from the previous session (it might be too big");
					g_main.drawingManager.clearTransparent();
					this.renderAllViewers();
					return;
				}
				g_main.drawingManager.setDepthImage(depthImg);
				canvas.width = canvas.height = 0;
			}
		}
	}

	getImageAsync(url){
		return new Promise((resolve, reject) => {
			let img = new Image();
			img.onload = _ => {
				resolve(img);
			}
			img.onerror = _ => {
				reject(img);
			}
			img.onResize = reject;
			img.src = url;
		});
	}

	fullRender(){
		if(!this.img) return;
		g_main.workerPool.clearAllCurrentRenders();
		this.renderAreasWithTimeout(0,0,this.img.width, this.img.height);
	}

	cancelRenderTimeout(){
		this.renderWhenReadyTimeout.stop();
	}

	renderAreasWithTimeout(x,y,w,h){
		let x2 = x+w;
		let y2 = y+h;
		if(!this.renderWhenReadyArea){
			this.renderWhenReadyArea = [x,y,x2,y2];
		}else{
			this.renderWhenReadyArea[0] = Math.min(this.renderWhenReadyArea[0], x);
			this.renderWhenReadyArea[1] = Math.min(this.renderWhenReadyArea[1], y);
			this.renderWhenReadyArea[2] = Math.max(this.renderWhenReadyArea[2], x2);
			this.renderWhenReadyArea[3] = Math.max(this.renderWhenReadyArea[3], y2);
		}
		this.renderWhenReadyTimeout.start();
	}

	clampXYWH(x,y,w,h){
		x = Math.floor(x);
		y = Math.floor(y);
		w = Math.ceil(w);
		h = Math.ceil(h);

		x = Math.max(x, 0);
		y = Math.max(y, 0);
		w = Math.min(w, this.rawCanvas.width - x);
		h = Math.min(h, this.rawCanvas.height - y);
		return [x,y,w,h];
	}

	cancelOverlappingRenderIds(x,y,w,h){
		[x,y,w,h] = this.clampXYWH(x,y,w,h);

		let ids = [];

		for(const area of this.renderingAreas){
			let isOverlapping = x < area.x + area.w && x+w > area.x && y < area.y + area.h && y+h > area.y;
			if(isOverlapping){
				x = Math.min(x, area.x);
				y = Math.min(y, area.y);
				let x2 = x+w;
				let y2 = y+h;
				let x3 = area.x+area.w;
				let y3 = area.y+area.h;
				let x4 = Math.max(x2, x3);
				let y4 = Math.max(y2, y3);
				w = x4 - x;
				h = y4 - y;
				ids.push(...area.ids);
			}
		}
		g_main.workerPool.cancelRenderIds(ids);

		return [x,y,w,h];
	}

	renderAreas(x,y,w,h){
		[x,y,w,h] = this.clampXYWH(x,y,w,h);

		const rowHeight = 200;
		let areaIds = [];
		for(let i=0; i < h; i += rowHeight){
			let newIds = this.renderArea(x,i+y,w, Math.min(h - i, rowHeight));
			areaIds.push(...newIds);
		}
		this.renderingAreas.push({
			x: x,
			y: y,
			w: w,
			h: h,
			ids: areaIds,
		});
	}

	renderArea(x,y,w,h){
		let leftId = this.renderAreaLR(x,y,w,h,true);
		let rightId = this.renderAreaLR(x,y,w,h,false);
		return [leftId, rightId];
	}

	renderAreaLR(x,y,w,h,left){
		let garbagePadding = Math.ceil(this.depthAmount * 150);
		let brushPadding = Math.ceil(this.depthAmount * 150);
		let totalPadding = garbagePadding + brushPadding;
		x -= totalPadding;
		w += totalPadding*2;
		[x,y,w,h] = this.clampXYWH(x,y,w,h);
		let rgbArr = this.rawCtx.getImageData(x,y,w,h).data;
		let depthArr = g_main.drawingManager.ctx.getImageData(x,y,w,h).data;
		let rgb = rgbArr.buffer;
		let depth = depthArr.buffer;
		let viewer = left ? this.leftViewer : this.rightViewer;
		viewer.clearRenderArea(x + garbagePadding,y,w - garbagePadding*2,h);
		let renderId = g_main.workerPool.renderArea(rgb, depth, w, h, this.depthAmount * (left ? 1 : -1));
		this.updateRenderedDataWhenDone(renderId, x, y, w, h, viewer, garbagePadding);
		return renderId;
	}

	async updateRenderedDataWhenDone(renderId, x, y, w, h, viewer, padding){
		let resultRgbArr = await g_main.workerPool.waitForRenderId(renderId);
		if(resultRgbArr){
			let resultRgbBuffer = new Uint8ClampedArray(resultRgbArr);
			let resultRgb = new ImageData(resultRgbBuffer, w, h);
			let leftPadding = x <= 0 ? 0 : padding;
			let rightPadding = x + w >= this.rawCanvas.width ? 0 : padding;
			viewer.updateRenderedData(resultRgb, x, y, leftPadding, rightPadding);
		}

		for(const area of this.renderingAreas){
			area.ids = area.ids.filter(id => id != renderId);
		}
		this.renderingAreas = this.renderingAreas.filter(area => area.ids.length > 0);
	}

	panViewers(x, y){
		this.viewportOffset[0] += x;
		this.viewportOffset[1] += y;
		this.saveViewport();
		this.renderAllViewers();
	}

	zoomViewers(deltaZoom, mouseX, mouseY, forceLeftRight){
		deltaZoom *= 0.005;
		let prevMousePos = this.screenPosToRenderPos([mouseX, mouseY], forceLeftRight);
		this.viewportZoomRaw -= deltaZoom;
		this.viewportZoomRaw = Math.max(0.01, this.viewportZoomRaw);
		let newMousePos = this.screenPosToRenderPos([mouseX, mouseY], forceLeftRight);
		let deltaMousePos = [newMousePos[0] - prevMousePos[0], newMousePos[1] - prevMousePos[1]];
		this.viewportOffset[0] += deltaMousePos[0] * this.viewportZoom;
		this.viewportOffset[1] += deltaMousePos[1] * this.viewportZoom;
		g_main.brushControls.updateCursorSize();
		this.saveViewport();
		this.renderAllViewers();
	}

	centerViewport(){
		this.viewportZoomRaw = Math.sqrt((window.innerWidth/2) / this.img.width);
		let y = (window.innerHeight - this.img.height*this.viewportZoom)/2;
		this.viewportOffset = [0,y];
		g_main.brushControls.updateCursorSize();
		this.saveViewport();
		this.renderAllViewers();
	}

	renderAllViewers(){
		this.renderRgbViewers();
		this.renderDepthViewers();
	}

	renderRgbViewers(){
		this.leftViewer.renderViewer();
		this.rightViewer.renderViewer();
	}

	renderDepthViewers(){
		this.leftDepthViewer.renderViewer();
		this.rightDepthViewer.renderViewer();
	}

	setDepthOpacity(opacity){
		this.leftDepthViewer.setOpacity(opacity);
		this.rightDepthViewer.setOpacity(opacity);
	}

	screenPosToRenderPos(pos, leftRight, doDepthOffset = true, fixLeftRightCoord = true){
		let x = pos[0];
		let y = pos[1];
		if(!leftRight){
			leftRight = x < window.innerWidth/2 ? "left" : "right";
		}
		if(leftRight == "right" && fixLeftRightCoord){
			x -= window.innerWidth/2;
		}
		if(doDepthOffset) x -= this.getViewerDepthOffset(leftRight);

		x -= this.viewportOffset[0];
		y -= this.viewportOffset[1];
		x /= this.viewportZoom;
		y /= this.viewportZoom;
		return [x,y];
	}
}
