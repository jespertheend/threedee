class FileUploader{
	constructor(){
		this.el = document.createElement("input");
		this.el.type = "file";
		this.el.accept = "image/*";
		this.el.addEventListener("change", e => {
			this.loadFile(this.el.files[0]);
			this.el.value = "";
		});

		this.img = null;

		this.onImageLoadedCbs = [];
		this.forceDepthMapNextFile = false;

		this.uploadScreen = document.getElementById("uploadScreen");
		this.uploadScreen.addEventListener("click", _ => {
			this.showFileSelect();
		});

		document.addEventListener("drop", e => {
			e.preventDefault();
			this.loadFile(e.dataTransfer.files[0]);
		});
		document.addEventListener("dragover", e => {
			e.preventDefault();
		});
	}

	showFileSelect(forceDepthMap){
		this.forceDepthMapNextFile = !!forceDepthMap;
		this.el.click();
	}

	loadFile(file){
		if(!file) return;
		let reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = e => {
			this.img = new Image();
			this.img.onload = _ => {
				this.onImageLoad();
			}
			this.img.src = e.target.result;
		}
	}

	testIsDepthMap(img, minPercent = 0.9){
		let canvas = document.createElement("canvas");
		let ctx = canvas.getContext("2d");
		if(CanvasWarningManager.testContext(ctx)) return;
		canvas.width = img.width;
		canvas.height = img.height;
		ctx.drawImage(img, 0, 0);
		let samples = 50;
		let greyCount = 0;
		for(let i=0; i<samples; i++){
			let x = Util.randInt(0, img.width);
			let y = Util.randInt(0, img.height);
			let rgb = ctx.getImageData(x,y,1,1).data;
			if(Math.abs(rgb[0] - rgb[1]) < 2 && Math.abs(rgb[1] - rgb[2]) < 2){
				greyCount++;
			}
		}
		canvas.width = canvas.height = 0;
		let greyPercent = greyCount/samples;
		return greyPercent > minPercent;
	}

	onImageLoad(){
		let isDepthMap = g_main.viewerManager.img && (this.forceDepthMapNextFile || this.testIsDepthMap(this.img));
		if(isDepthMap){
			g_main.drawingManager.setDepthImage(this.img);
		}else{
			g_main.viewerManager.setFullImage(this.img);
		}
		this.img = null;
		this.setLoadedImageEls();
		for(const cb of this.onImageLoadedCbs){
			cb();
		}
		this.onImageLoadedCbs = [];
	}

	onImageLoaded(cb){
		this.onImageLoadedCbs.push(cb);
	}

	randInt(min, max) {
		return Math.floor(this.randRange(min, max));
	}

	setLoadedImageEls(){
		if(g_main.viewerManager.img){
			this.uploadScreen.style.display = "none";
			g_main.brushControls.touchListener.style.display = null;
			g_main.ui.showUI();
		}else{
			this.uploadScreen.style.display = null;
			g_main.brushControls.touchListener.style.display = "none";
			g_main.ui.hideUI();
		}
	}
}
