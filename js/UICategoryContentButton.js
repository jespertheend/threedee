class UICategoryContentButton extends UIButton{
	constructor(opts){
		opts = {...{
			visibleInCategories: [],
		}, ...opts}
		super(opts);
		this.visibleInCategories = opts.visibleInCategories;

		this.visible = false;
	}

	setVisible(){
		this.visible = this.visibleInCategories.indexOf(g_main.ui.activeCategory) >= 0;
		for(const el of this.els){
			el.el.style.visibility = this.visible ? null : "hidden";
		}
		this.setPositionOffset(0, this.visible ? 0 : 80);
	}
}
