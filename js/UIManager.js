class UIManager{
	constructor(){
		this.leftContainer = document.createElement("div");
		this.leftContainer.classList.add("menuContainer");
		this.leftContainer.style.display = "none";
		document.body.appendChild(this.leftContainer);
		this.rightContainer = document.createElement("div");
		this.rightContainer.classList.add("menuContainer");
		this.rightContainer.classList.add("right");
		this.rightContainer.style.display = "none";
		document.body.appendChild(this.rightContainer);

		this.categoryMenuOpen = false;
		this.activeCategory = UIManager.Categories.BRUSH_ADDITIVE;
	}

	static get Categories(){
		return {
			BRUSH_ADDITIVE: 0,
			BRUSH_ABSOLUTE: 1,
			COLOR_PICKER: 2,
			BRUSH_BLUR: 3,
			BRUSH_SHARPEN: 4,
			SETTINGS: 5,
		};
	}

	init(){
		// this.undoButton = new UIButton({
		// 	left: 10,
		// 	top: 10,
		// 	label: "undo",
		// });
		// this.redoButton = new UIButton({
		// 	left: 70,
		// 	top: 10,
		// 	label: "redo",
		// });
		this.settingsButton = new UIButton({
			right: 10,
			top: 10,
			label: "settings",
			icon: "icons/settings.svg",
			onClick: _ => {
				this.setCategory(UIManager.Categories.SETTINGS);
			},
		});
		this.depthOpacityButton = new UIButton({
			right: 10,
			top: 70,
			label: "depth opacity",
			localStorageName: "DepthOpacity",
			isSlider: true,
			startValue: 0.4,
			minValue: 0,
			maxValue: 1,
			moveSpeed: 0.01,
			decimals: 2,
			onValueChange: value => {
				g_main.viewerManager.setDepthOpacity(value);
			},
		});
		this.view2dButton = new UIButton({
			right: 10,
			top: 130,
			label: "2d view",
			localStorageName: "2dView",
			isToggle: true,
			startValue: false,
			onValueChange: value => {
				g_main.viewerManager.renderRgbViewers();
			},
		});
		this.centerViewportButton = new UIButton({
			right: 70,
			top: 10,
			label: "center viewport",
			icon: "icons/centerViewport.svg",
			onClick: _ => {
				g_main.viewerManager.centerViewport();
			},
		});

		//categories
		this.changeBrushButton = new UIButton({
			left: 10,
			bottom: 10,
			label: "change brush",
			onClick: _ => {
				this.categoryMenuOpen = true;
				this.setCategoryButtonPositions();
				this.setExportButtonsVisible(false);
			}
		});
		this.categoryButtons = [
			this.brushAdditive = new UICategoryButton({
				left: 10,
				bottom: 10,
				category: UIManager.Categories.BRUSH_ADDITIVE,
				label: "additive",
				icon: "icons/additiveBrush.svg",
			}),
			this.brushAbsolute = new UICategoryButton({
				left: 10,
				bottom: 10,
				category: UIManager.Categories.BRUSH_ABSOLUTE,
				label: "absolute",
				icon: "icons/absoluteBrush.svg",
			}),
			this.brushBlur = new UICategoryButton({
				left: 10,
				bottom: 10,
				category: UIManager.Categories.BRUSH_BLUR,
				label: "blur",
				icon: "icons/blurBrush.svg",
			}),
			this.brushSharpen = new UICategoryButton({
				left: 10,
				bottom: 10,
				category: UIManager.Categories.BRUSH_SHARPEN,
				label: "sharpen",
				icon: "icons/sharpenBrush.svg",
			}),
			this.colorPicker = new UICategoryButton({
				left: 10,
				bottom: 10,
				category: UIManager.Categories.COLOR_PICKER,
				label: "color picker",
				icon: "icons/pickColor.svg",
			}),
		]

		this.categoryContentButtons = [
			this.brushSizeButton = new UICategoryContentButton({
				left: 70,
				bottom: 10,
				visibleInCategories: [UIManager.Categories.BRUSH_ADDITIVE, UIManager.Categories.BRUSH_ABSOLUTE, UIManager.Categories.BRUSH_BLUR, UIManager.Categories.BRUSH_SHARPEN],
				label: "brush size",
				localStorageName: "BrushSize",
				isSlider: true,
				startValue: 10,
				minValue: 0,
				moveSpeed: 0.1,
				decimals: 1,
				onValueChange: (newValue, fromUserAction) => {
					if(fromUserAction){
						g_main.brushControls.updateCursorSize();
						g_main.brushControls.centerCursor();
					}
					g_main.drawingManager.endBrush();
				}
			}),
			this.brushSoftnessButton = new UICategoryContentButton({
				left: 130,
				bottom: 10,
				visibleInCategories: [UIManager.Categories.BRUSH_ADDITIVE, UIManager.Categories.BRUSH_ABSOLUTE],
				label: "brush softness",
				localStorageName: "BrushSoftness",
				isSlider: true,
				startValue: 0,
				minValue: 0,
				maxValue: 1,
				moveSpeed: 0.003,
				decimals: 2,
				onValueChange: (newValue, fromUserAction) => {
					if(fromUserAction){
						g_main.brushControls.updateCursorSize();
						g_main.brushControls.centerCursor();
					}
					g_main.drawingManager.endBrush();
				}
			}),
			this.brushBlurAmountButton = new UICategoryContentButton({
				left: 130,
				bottom: 10,
				visibleInCategories: [UIManager.Categories.BRUSH_BLUR],
				label: "blur amount",
				localStorageName: "BlurAmount",
				isSlider: true,
				startValue: 0.5,
				minValue: 0,
				maxValue: 10,
				moveSpeed: 0.003,
				decimals: 2,
				onValueChange: _ => {
					g_main.drawingManager.endBrush();
				}
			}),
			this.brushAddAmountButton = new UICategoryContentButton({
				left: 190,
				bottom: 10,
				visibleInCategories: [UIManager.Categories.BRUSH_ADDITIVE],
				label: "brush depth add amount",
				localStorageName: "BrushDepthAddAmount",
				isSlider: true,
				startValue: 0.1,
				minValue: -1,
				maxValue: 1,
				moveSpeed: 0.0005,
				decimals: 4,
				onValueChange: _ => {
					g_main.drawingManager.endBrush();
				}
			}),
			this.brushOpacityButton = new UICategoryContentButton({
				left: 190,
				bottom: 10,
				visibleInCategories: [UIManager.Categories.BRUSH_ABSOLUTE],
				label: "brush opacity",
				localStorageName: "BrushOpacity",
				isSlider: true,
				startValue: 1,
				minValue: 0,
				maxValue: 1,
				moveSpeed: 0.003,
				decimals: 2,
				onValueChange: _ => {
					g_main.drawingManager.endBrush();
				}
			}),
			this.brushValueButton = new UICategoryContentButton({
				left: 250,
				bottom: 10,
				visibleInCategories: [UIManager.Categories.BRUSH_ABSOLUTE],
				label: "brush value",
				localStorageName: "BrushValue",
				isSlider: true,
				startValue: 0.5,
				minValue: 0,
				maxValue: 1,
				moveSpeed: 0.001,
				decimals: 3,
				onValueChange: _ => {
					g_main.drawingManager.endBrush();
				}
			}),

			this.alwaysPickColorButton = new UICategoryContentButton({
				left: 70,
				bottom: 10,
				visibleInCategories: [UIManager.Categories.COLOR_PICKER],
				label: "always pick color",
				localStorageName: "AlwaysPickColor",
				isToggle: true,
				startValue: false,
			}),

			//settings category
			this.settingsDepthAmountButton = new UICategoryContentButton({
				left: 70,
				bottom: 10,
				visibleInCategories: [UIManager.Categories.SETTINGS],
				label: "depth amount",
				localStorageName: "depthAmount",
				isSlider: true,
				startValue: 1,
				minValue: 0,
				decimals: 2,
				moveSpeed: 0.01,
				onValueChange: v => {
					g_main.viewerManager.fullRender();
					g_main.viewerManager.renderAllViewers();
				},
			}),
			this.settingsUploadDepthMapButton = new UICategoryContentButton({
				left: 130,
				bottom: 10,
				visibleInCategories: [UIManager.Categories.SETTINGS],
				label: "upload depth map",
				icon: "icons/upload.svg",
				onClick: _ => {
					g_main.fileUploader.showFileSelect(true);
				}
			}),
			this.settingsInvertDepthMapButton = new UICategoryContentButton({
				left: 190,
				bottom: 10,
				visibleInCategories: [UIManager.Categories.SETTINGS],
				label: "invert depth map",
				icon: "icons/invert.svg",
				onClick: _ => {
					g_main.drawingManager.invertDepthMap();
				}
			}),
			this.settingsExportButton = new UICategoryContentButton({
				left: 250,
				bottom: 10,
				visibleInCategories: [UIManager.Categories.SETTINGS],
				label: "export",
				icon: "icons/export.svg",
				onClick: _ => {
					this.setExportButtonsVisible(true);
				}
			}),
			this.settingsNewImageButton = new UICategoryContentButton({
				left: 310,
				bottom: 10,
				visibleInCategories: [UIManager.Categories.SETTINGS],
				label: "new image",
				icon: "icons/newImage.svg",
				onClick: async _ => {
					if(confirm("Any unsaved data will be lost. Are you sure?")){
						await g_main.viewerManager.clearFullImage();
						window.location.reload();
					}
				}
			}),
		];

		this.exportButtons = [
			this.exportCrossViewButton = new UIButton({
				left: 250,
				bottom: 190,
				label: "export crossview",
				onClick: _ => {
					g_main.exportManager.exportCrossView();
				}
			}),
			this.exportParallelViewButton = new UIButton({
				left: 250,
				bottom: 130,
				label: "export parallelview",
				onClick: _ => {
					g_main.exportManager.exportParallelView();
				}
			}),
			this.exportDepthButton = new UIButton({
				left: 250,
				bottom: 70,
				label: "export depth",
				onClick: _ => {
					g_main.exportManager.exportDepthView();
				}
			}),
		];

		this.setCategoryFromLocalStorage();
		this.setExportButtonsVisible(false);
	}

	async setCategoryFromLocalStorage(){
		let value = await IndexedDbUtil.get("activeCategory");
		if(value === undefined) value = UIManager.Categories.BRUSH_ADDITIVE;
		this.setCategory(value);
	}

	showUI(){
		this.leftContainer.style.display = null;
		this.rightContainer.style.display = null;
	}

	hideUI(){
		this.leftContainer.style.display = "none";
		this.rightContainer.style.display = "none";
	}

	onCategoryClick(category){
		if(this.categoryMenuOpen){
			this.setCategory(category);
		}
		this.categoryMenuOpen = !this.categoryMenuOpen;
		this.setCategoryButtonPositions();
		this.setExportButtonsVisible(false);
	}

	setCategoryButtonPositions(){
		let activeIcon = "";
		for(let i=0; i<this.categoryButtons.length; i++){
			let button = this.categoryButtons[i];
			let yPos = 80;
			let visible = false;
			if(this.categoryMenuOpen){
				yPos = (i - this.categoryButtons.length + 1)*60;
				visible = true;
			}else if(button.category == this.activeCategory){
				activeIcon = button.icon;
			}
			button.setPositionOffset(0, yPos);
			for(const el of button.els){
				el.el.style.visibility = visible ? null : "hidden";
			}
		}
		this.changeBrushButton.setPositionOffset(0, this.categoryMenuOpen ? 80 : 0);
		if(this.activeCategory == UIManager.Categories.SETTINGS) activeIcon = this.settingsButton.icon;
		this.changeBrushButton.setIcon(activeIcon);
	}

	setCategory(category){
		this.activeCategory = category;
		IndexedDbUtil.set("activeCategory", category);
		this.setCategoryButtonPositions();
		for(const button of this.categoryContentButtons){
			button.setVisible();
		}
	}

	setExportButtonsVisible(visible){
		for(const button of this.exportButtons){
			button.setPositionOffset(0, visible ? 0 : 250);
			for(const el of button.els){
				el.el.style.visibility = visible ? null : "hidden";
			}
		}
	}
}
