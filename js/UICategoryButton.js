class UICategoryButton extends UIButton{
	constructor(opts){
		opts = {...{
			category: UIManager.Categories.BRUSH_ADDITIVE,
			onClick: _ => {
				g_main.ui.onCategoryClick(this.category);
			}
		}, ...opts}
		super(opts);
		this.category = opts.category;
	}
}
