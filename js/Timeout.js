class Timeout{
	constructor(cb, ms, startAtCreation){
		this.cb = cb;
		this.id = -1;
		this.ms = ms;
		this.isDestructed = false;

		if(startAtCreation)
			this.start();
	}

	destructor(){
		this.stop();
		this.isDestructed = true;
		this.cb = null;
	}

	get isRunning(){
		return this.id != -1;
	}

	//returns true if the timeout was running and is now cleared,
	//returns false if there was not timeout running
	stop(){
		if(this.isDestructed) return;
		if(this.id >= 0){
			window.clearTimeout(this.id);
			this.id = -1;
			return true;
		}
		return false;
	}

	start(){
		if(this.isDestructed) return;
		this.stop();
		this.id = window.setTimeout(this.execute.bind(this), this.ms);
	}

	execute(){
		this.id = -1;
		if(this.cb) this.cb();
	}

	static promise(ms){
		return new Promise((resolve, reject) => {
			window.setTimeout(() => {
				resolve();
			}, ms);
		});
	}
}
