class UIButton{
	constructor(opts){
		opts = {...{
			left: null,
			top: null,
			right: null,
			bottom: null,
			posOffset: [0, 0],
			onClick: null,
			onValueChange: null,
			label: "",
			localStorageName: null,
			isSlider: false,
			startValue: 0,
			minValue: null,
			maxValue: null,
			moveSpeed: 1,
			decimals: 0,
			isToggle: false,
			icon: null,
		}, ...opts}
		this.icon = opts.icon;

		this.leftEls = this.createDom(true);
		this.rightEls = this.createDom(false);


		this.els = [this.leftEls, this.rightEls];

		this.setIcon(this.icon);
		for(const els of this.els){
			if(opts.left != null) els.el.style.left = opts.left+"px";
			if(opts.right != null) els.el.style.right = opts.right+"px";
			if(opts.top != null) els.el.style.top = opts.top+"px";
			if(opts.bottom != null) els.el.style.bottom = opts.bottom+"px";
			els.label.textContent = opts.label;
		}
		this.isHovering = false;

		this.isDragging = false;
		this._sliderValue = 0;
		this._toggleValue = false;

		this.posOffset = opts.posOffset;
		this.onClick = opts.onClick;
		this.onValueChange = opts.onValueChange;
		this._localStorageName = opts.localStorageName;
		this.isSlider = opts.isSlider;
		this.startValue = opts.startValue;
		this.isToggle = opts.isToggle;
		this.decimals = opts.decimals;
		this.minValue = opts.minValue;
		this.maxValue = opts.maxValue;
		this.moveSpeed = opts.moveSpeed;
		if(this.isSlider || this.isToggle){
			if(this.localStorageName){
				this.restoreLocalStorageValue();
			}else{
				if(this.isSlider){
					this.sliderValue = opts.startValue;
				}else{
					this.toggleValue = opts.startValue;
				}
			}
		}

		this.addBothEventListeners("mouseenter", _ => {
			this.onMouseEnter();
		});
		this.addBothEventListeners("mouseleave", _ => {
			this.onMouseLeave();
		});
		this.addBothEventListeners("mousedown", e => {
			e.preventDefault();
			this.isDragging = true;
		});
		window.addEventListener("mousemove", e => {
			if(this.isDragging && this.isSlider){
				e.preventDefault();
				let newValue = this.sliderValue;
				newValue += e.movementX * this.moveSpeed;
				newValue -= e.movementY * this.moveSpeed;
				this.setSliderValue(newValue, true);
			}
		});
		window.addEventListener("mouseup", _ => {
			this.isDragging = false;
		});
		this.addBothEventListeners("click", _ => {
			this.click();
		});
		this.addBothEventListeners("wheel", e => {
			if(this.isSlider){
				e.preventDefault();
				e.stopPropagation();
				let newValue = this.sliderValue + e.deltaY * this.moveSpeed;
				this.setSliderValue(newValue, true);
			}
		});
		this.currentTouch = null;
		this.addBothEventListeners("touchstart", e => {
			e.preventDefault();
			this.currentTouch = e.touches[0];
		}, {passive: false});
		window.addEventListener("touchmove", e => {
			if(this.currentTouch && this.isSlider){
				for(const touch of e.touches){
					if(this.currentTouch.identifier == touch.identifier){
						let deltaX = this.currentTouch.screenX - touch.screenX;
						let deltaY = this.currentTouch.screenY - touch.screenY;
						let newValue = this.sliderValue;
						newValue -= deltaX * this.moveSpeed;
						newValue += deltaY * this.moveSpeed;
						this.setSliderValue(newValue, true);
						this.currentTouch = touch;
					}
				}
			}
		}, {passive: false});
		window.addEventListener("touchend", e => {
			this.onTouchEnd(e);
		}, {passive: false});
		window.addEventListener("touchcancel", e => {
			this.onTouchEnd(e);
		}, {passive: false});

		this.updateCssPosition();
	}

	createDom(left){
		let el = document.createElement("div");
		el.classList.add("circleButton");
		let label = document.createElement("div");
		label.classList.add("circleButtonLabel");
		el.appendChild(label);
		let valueLabel = document.createElement("div");
		valueLabel.classList.add("circleButtonLabel");
		valueLabel.classList.add("circleButtonValueLabel");
		el.appendChild(valueLabel);
		let iconEl = document.createElement("div");
		iconEl.classList.add("circleButtonIcon");
		iconEl.style.backgroundSize = "50%";
		el.appendChild(iconEl);
		if(left){
			el.classList.add("left");
			g_main.ui.leftContainer.appendChild(el);
		}else{
			el.classList.add("right");
			g_main.ui.rightContainer.appendChild(el);
		}

		return {
			el: el,
			label: label,
			valueLabel: valueLabel,
			iconEl: iconEl,
		}
	}

	setIcon(icon){
		let val = icon ? `url(${icon})` : null;
		for(const els of this.els){
			els.iconEl.style.backgroundImage = val;
		}
	}

	get localStorageName(){
		if(!this._localStorageName) return null;
		return "sliderButton"+this._localStorageName+"Value";
	}

	async restoreLocalStorageValue(){
		let value = await IndexedDbUtil.get(this.localStorageName);
		if(this.isSlider){
			if(value !== undefined){
				this.sliderValue = value;
			}else{
				this.sliderValue = this.startValue;
			}
		}else if(this.isToggle){
			if(value !== undefined){
				this.toggleValue = value;
			}else{
				this.toggleValue = this.startValue;
			}
		}
	}

	get sliderValue(){
		return this._sliderValue;
	}

	set sliderValue(value){
		this.setSliderValue(value);
	}

	setSliderValue(value, fromUserAction = false){
		this._sliderValue = value;
		if(this.minValue != null) this._sliderValue = Math.max(this._sliderValue, this.minValue);
		if(this.maxValue != null) this._sliderValue = Math.min(this._sliderValue, this.maxValue);
		this.renderSliderValue();
		if(this.localStorageName){
			IndexedDbUtil.set(this.localStorageName, this._sliderValue);
		}
		if(this.onValueChange) this.onValueChange(this.sliderValue, fromUserAction);
	}

	get toggleValue(){
		return this._toggleValue;
	}

	set toggleValue(value){
		this._toggleValue = value;
		this.renderSliderValue();
		if(this.localStorageName){
			IndexedDbUtil.set(this.localStorageName, this._toggleValue);
		}
		if(this.onValueChange) this.onValueChange(this.toggleValue);
	}

	addBothEventListeners(name, cb, opts){
		this.leftEls.el.addEventListener(name, cb, opts);
		this.rightEls.el.addEventListener(name, cb, opts);
	}

	setPositionOffset(x,y){
		this.posOffset = [x,y];
		this.updateCssPosition();
	}

	updateCssPosition(){
		let depth = 1;
		if(this.isHovering) depth += 3;
		this.leftEls.el.style.transform = `translate(${-depth + this.posOffset[0]}px, ${this.posOffset[1]}px)`;
		this.rightEls.el.style.transform = `translate(${depth + this.posOffset[0]}px, ${this.posOffset[1]}px)`;
	}

	onMouseEnter(){
		this.isHovering = true;
		this.updateCssPosition();
	}

	onMouseLeave(){
		this.isHovering = false;
		this.updateCssPosition();
	}

	onTouchEnd(e){
		if(this.currentTouch){
			let exists = false;
			for(const touch of e.touches){
				if(this.currentTouch.identifier == touch.identifier){
					exists = true;
					break;
				}
			}
			if(!exists){
				this.currentTouch = null;
				this.click();
			}
		}
	}

	click(){
		if(this.isToggle){
			this.toggleValue = !this.toggleValue;
		}
		if(this.onClick) this.onClick();
	}

	renderSliderValue(){
		if(this.isSlider){
			let decimals = Math.pow(10, this.decimals);
			let value = ""+Math.round(this.sliderValue*decimals)/decimals;
			if(value){
				let split = value.split(".");
				if(split.length > 1){
					let currentDecimalCount = split[1].length;
					if(currentDecimalCount < this.decimals){
						let diff = this.decimals - currentDecimalCount;
						for(let i=0; i<diff; i++){
							value = value + "0";
						}
					}
				}
			}
			for(const els of this.els){
				els.valueLabel.textContent = value;
			}
		}else if(this.isToggle){
			let value = this.toggleValue;
			for(const els of this.els){
				els.valueLabel.textContent = value ? "yes" : "no";
			}
		}
	}
}
